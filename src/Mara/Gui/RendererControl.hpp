#pragma once
#include "ui_RendererControl.h"
#include <functional>
#include <vector>

#include <RadiumNBR/Gui/RendererPanel.hpp>

namespace Ra::Engine::Rendering {
class Renderer;
}
namespace Mara {
/**
 * Gui to manage renderers.
 *
 * Allow to switch from a renderer to the other and to select renderer outputs (textures).
 *
 * When a renderer is activated, this control is filled by the renderer specific control panel in
 * addition of the renderer common controlers.
 *
 * This gui presents the following interaction elements
 *  - Renderers combo box : filled by the set of rmanaged renderers, this combo box allow to
 *      switch from one renderer to another.
 *  - Camera buttons : two button, independant from the renderer, that allow to manage the active
 *      camera.
 *  - Renderer specific control : This control contains two things :
 *      - for any renderer, a combox to select the displayed AOV produced by the renderer.
 *      - A control panel specific to the active renderer to manage its internal state
 */
class RendererControl : public QFrame, private Ui::RendererControler
{
    Q_OBJECT

  public:
    /** Constructors and destructor.
     *  https://en.cppreference.com/w/cpp/language/rule_of_three
     */
    /** @{ */
    explicit RendererControl( QWidget* parent = nullptr );
    RendererControl( const RendererControl& ) = delete;
    RendererControl& operator=( const RendererControl& ) = delete;
    RendererControl( RendererControl&& )                 = delete;
    RendererControl& operator=( RendererControl&& ) = delete;
    ~RendererControl() override                     = default;
    /**@}*/

    /**
     * Add a renderer to the control panel
     * @param rendererName The name that will appear in the renderer selection combobox.
     * @param renderer A pointer to the renderer to add. The ownership is left to the caller.
     * @param callback The function to call to activate a renderer. Must return true if the
     * activation succeed.
     * @param controlPanel The renderer specific control gui.
     */
    void addRenderer( const std::string& rendererName,
                      Ra::Engine::Rendering::Renderer* constrenderer,
                      std::function<bool()> callback,
                      RadiumNBR::Gui::RendererPanel* controlPanel = nullptr );

  signals:
    /// Signal emitted when an internal state of the active renderer  was changed.
    void rendererStateChanged();

    /// Signal emitted when the "fit camera" button is pressed
    void fitCamera();
    /// Signal emitted when the "Reset camera" is pressed
    void resetCamera();

  private slots:
    /// Slot for renderer list combo
    void changeRenderer( const std::string& renderer );

    /// Slot for texture list combo
    void changeTexture( const std::string& texture );

  private:
    /// The association between a renderer and its activation function
    using RendererInfo = std::pair<Ra::Engine::Rendering::Renderer* const, std::function<bool()>>;
    /// The active renderer
    int m_currentRenderer{-1};

    /// The Renderers/callbacks managed by the gui
    std::vector<RendererInfo> m_renderersCallbacks;
    /// The renderers specific control gui
    std::vector<RadiumNBR::Gui::RendererPanel*> m_renderersPanels;

    /// combo bos to select one renderer output for display
    QComboBox* m_textureList{nullptr};
};
} // namespace Mara
