#pragma once

#include <Core/Asset/FileLoaderInterface.hpp>
#include <RadiumH3D/h3DMacros.hpp>

#include <RadiumH3D/h3DLoader.hpp>

namespace H3D {
h3D_LIBRARY_API void h3DLibraryInfo();
} // namespace H3D
