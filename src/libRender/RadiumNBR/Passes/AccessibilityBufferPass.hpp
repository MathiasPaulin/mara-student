#pragma once
#include <RadiumNBR/RenderPass.hpp>
#include <RadiumNBR/SphereSampler.hpp>

#include <Engine/Data/DisplayableObject.hpp>

namespace Ra::Engine::Data {
class ShaderProgram;
} // namespace Ra::Engine::Data

namespace globjects {
class Framebuffer;
}

namespace RadiumNBR {
/// This pass compute the accessibility buffer using SSDO algorithm.
/// It takes both a WorlPosision and WorldNormal textures and compute the
/// Accessobility texture at each visible point.
class AccessibilityBufferPass : public RenderPass
{
  public:
    AccessibilityBufferPass( const std::vector<RenderObjectPtr>* objectsToRender,
                             const Ra::Core::Utils::Index& idx );
    ~AccessibilityBufferPass() override;

    bool buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                               Ra::Engine::Rendering::RenderTechnique& rt ) const override;
    bool initializePass( size_t width,
                         size_t height,
                         Ra::Engine::Data::ShaderProgramManager* shaderMngr ) override;
    bool update() override;
    void resize( size_t width, size_t height ) override;
    void execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const override;

    /// These inputs must be computed before executing this pass
    /// positions and normals must be computed in world space
    void setInputs( const SharedTextures& positions, const SharedTextures& normals );

    /// Set the sampler to use to compute SSDO
    void setSampler( std::unique_ptr<SphereSampler> sampler );

    /// Get the SSDO radius
    Scalar aoRadius() { return m_aoRadius; }

    /// Set the SSDO radius
    void setAoRadius( Scalar rds ) { m_aoRadius = rds; }

  private:
    /// The framebuffer used to render this pass
    std::unique_ptr<globjects::Framebuffer> m_fbo{nullptr};

    /// The quad to be drawn for shader invocation
    std::unique_ptr<Ra::Engine::Data::Displayable> m_quadMesh{nullptr};

    /// The shader that computes ssdo (the Radium shader manager has ownership)
    const Ra::Engine::Data::ShaderProgram* m_shader{nullptr};

    /// The shader that blurs ssdo (the Radium shader manager has ownership)
    const Ra::Engine::Data::ShaderProgram* m_blurShader{nullptr};

    std::unique_ptr<SphereSampler> m_sphereSampler;

    /// Ambiant occlusion Radius, in % of the scene bbox
    Scalar m_aoRadius{5};

    /// Dimension of the scene. Used to adapt the aoRadius to the scene dimensions.
    Scalar m_sceneDiag{1};

    /// Temporary texture for raw AO computation
    std::unique_ptr<Ra::Engine::Data::Texture> m_rawAO;
    /// The framebuffer used to blur the AO
    std::unique_ptr<globjects::Framebuffer> m_blurfbo{nullptr};
};
} // namespace RadiumNBR
