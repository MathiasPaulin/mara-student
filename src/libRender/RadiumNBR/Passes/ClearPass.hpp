#pragma once
#include <Core/Utils/Color.hpp>
#include <RadiumNBR/EnvMap.hpp>
#include <RadiumNBR/RenderPass.hpp>

#include <globjects/Framebuffer.h>

namespace Ra::Engine::Data {
class ShaderProgram;
} // namespace Ra::Engine::Data

namespace RadiumNBR {
/// This pass just clear its output to a background color
class ClearPass : public RenderPass
{
  public:
    ClearPass( const std::vector<RenderObjectPtr>* objectsToRender,
               const Ra::Core::Utils::Index& idx );
    ~ClearPass() override;

    bool buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                               Ra::Engine::Rendering::RenderTechnique& rt ) const override;
    bool initializePass( size_t width,
                         size_t height,
                         Ra::Engine::Data::ShaderProgramManager* shaderMngr ) override;
    bool update() override;
    void resize( size_t width, size_t height ) override;
    void execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const override;

    /// Add the output colorBuffer
    void setOutput( const SharedTextures& colorBuffer );

    /// Set the color background for the picture.
    void setBackground( const Ra::Core::Utils::Color& bgk );

    /// set the env map to use as background color
    void setEnvMap( std::shared_ptr<EnvMap> envmp );

    /// show/hide envmap
    void showEnvMap( bool state );

  private:
    /// The framebuffer used to render this pass
    std::unique_ptr<globjects::Framebuffer> m_fbo{nullptr};

    /// The Shader manager to use when building shaders
    Ra::Engine::Data::ShaderProgramManager* m_shaderMngr{nullptr};

    /// The color texture for output.Stored here for easy access.
    SharedTextures m_outputTexture;

    /// Background Color
    Ra::Core::Utils::Color m_bgkColor;

    /// The Environment to used for skybox display
    std::shared_ptr<EnvMap> m_envmap{nullptr};

    /// Is the environment drawn ?
    bool m_showEnvMap{true};

    int m_skyLod{0};
};
} // namespace RadiumNBR
