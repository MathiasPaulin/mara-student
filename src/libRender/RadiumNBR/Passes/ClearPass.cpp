#include "ClearPass.hpp"

#include <Engine/Rendering/RenderObject.hpp>

#include <Engine/Data/Texture.hpp>

#include <globjects/Framebuffer.h>

namespace RadiumNBR {
using namespace gl;

static const GLenum buffers[] = {GL_COLOR_ATTACHMENT0};

ClearPass::ClearPass( const std::vector<RenderObjectPtr>* objectsToRender,
                      const Ra::Core::Utils::Index& idx ) :
    RenderPass( "Clear pass", idx, objectsToRender ) {}

ClearPass::~ClearPass() = default;

bool ClearPass::initializePass( size_t /* width */,
                                size_t /* height */,
                                Ra::Engine::Data::ShaderProgramManager* shaderMngr ) {
    m_shaderMngr = shaderMngr;
    m_fbo        = std::make_unique<globjects::Framebuffer>();

    return true;
}

void ClearPass::setOutput( const SharedTextures& colorBuffer ) {
    m_outputTexture = colorBuffer;
}

bool ClearPass::update() {
    return true;
}

void ClearPass::resize( size_t width, size_t height ) {
    m_fbo->bind();
    m_fbo->attachTexture( GL_COLOR_ATTACHMENT0, m_outputTexture.second->texture() );

    // finished with fbo, undbind to bind default
    globjects::Framebuffer::unbind();
}

void ClearPass::execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const {
    m_fbo->bind();
    GL_ASSERT( glDrawBuffers( 1, buffers ) );
    GL_ASSERT( glDisable( GL_BLEND ) );
    // Draw sky if needed
    if ( m_envmap && m_envmap->isSkybox() )
    {
        GL_ASSERT( glDepthMask( GL_FALSE ) );
        GL_ASSERT( glColorMask( GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE ) );
        GL_ASSERT( glDisable( GL_DEPTH_TEST ) );
        m_envmap->render( viewParams );
        GL_ASSERT( glDepthMask( GL_TRUE ) );
        GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
    }
    else
    {
        GL_ASSERT( glClearBufferfv( GL_COLOR, 0, m_bgkColor.data() ) ); // Clear Color buffer
    }
}

bool ClearPass::buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                                      Ra::Engine::Rendering::RenderTechnique& rt ) const {
    return true;
}

void ClearPass::setBackground( const Ra::Core::Utils::Color& bgk ) {
    m_bgkColor = bgk;
}

void ClearPass::setEnvMap( std::shared_ptr<EnvMap> envmp ) {
    m_envmap = envmp;
    if ( m_envmap ) { m_envmap->setSkybox( m_showEnvMap ); }
}

void ClearPass::showEnvMap( bool state ) {
    m_showEnvMap = state;
    if ( m_envmap ) { m_envmap->setSkybox( m_showEnvMap ); }
}

} // namespace RadiumNBR
