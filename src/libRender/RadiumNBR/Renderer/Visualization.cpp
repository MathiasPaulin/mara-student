#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/RadiumEngine.hpp>
#include <RadiumNBR/Passes/ClearPass.hpp>
#include <RadiumNBR/Passes/CustomAttribToColorPass.hpp>
#include <RadiumNBR/Passes/GeomPrepass.hpp>
#include <RadiumNBR/Passes/ImageProcessPass.hpp>
#include <RadiumNBR/Renderer/Visualization.hpp>

#include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log

namespace RadiumNBR {

void VisualizationController::configure( RadiumNBR::NodeBasedRenderer* renderer, int w, int h ) {
    m_renderer = renderer;

    //! [Caching some helpers and data from the Engine and the renderer]
    auto resourcesCheck = Ra::Core::Resources::getResourcesPath(
        reinterpret_cast<void*>( &RadiumNBR::NodeBasedRendererMagic ), {"Resources/RadiumNBR"} );
    if ( !resourcesCheck )
    {
        LOG( Ra::Core::Utils::logERROR ) << "Unable to find resources for NodeBasedRenderer!";
        return;
    }
    auto resourcesPath{*resourcesCheck};
    auto shaderManager = Ra::Engine::RadiumEngine::getInstance()->getShaderProgramManager();
    auto colortexture  = renderer->sharedTextures().find( "Linear RGB (RadiumNBR)" );
    auto depthtexture  = renderer->sharedTextures().find( "Depth (RadiumNBR)" );
    //! [Caching some helpers and data from the Engine and the renderer]

    //! [Adding a clear-screen pass]
    {
        // pass that draw no object and is positioned at rank 0
        m_clearPass = std::make_shared<RadiumNBR::ClearPass>( nullptr, 0 );
        // set the output of the pass : clear the renderer Linear RGB output texture
        m_clearPass->setOutput( *colortexture );
        // set the clear color (pass internal state)
        m_clearPass->setBackground( renderer->getBackgroundColor() );
        m_clearPass->initializePass( w, h, shaderManager );
        // add the pass to the renderer and activate it
        renderer->addPass( m_clearPass, m_clearPass->index() );
        m_clearPass->activate();
    }
    //! [Adding a clear-screen pass]

    //! [Adding a CustomAttribToColorPass pass]
    {
        // this pass draw all the objects using the custom color function,
        // Rendering is done against the shared z-buffer for hidden line removal and
        // into the shared color buffer.
        m_customPass =
            std::make_shared<RadiumNBR::CustomAttribToColorPass>( renderer->allRenderObjects(), 2 );
        // set the input/output textures
        m_customPass->setInputs( *depthtexture );
        m_customPass->setOutput( *colortexture );
        m_customPass->setLightManager( renderer->getLightManager() );
        m_customPass->setAttribToColorFunc(
            m_vertexFunction, m_geometryFunction, m_fragmentFunction );
        // configure access to shader/resources files and initialize the pass
        m_customPass->setResourcesDir( resourcesPath );
        m_customPass->initializePass( w, h, shaderManager );
        // add the pass to the renderer and activate it
        renderer->addPass( m_customPass, m_customPass->index() );
        // Add the exported (shared) texture to the set of available textures
        auto &sharedTextures = renderer->sharedTextures();
        for (auto t : m_customPass->getOutputImages() ) {
            sharedTextures.insert( {t.first, t.second} );
        }
        m_customPass->activate();
    }
    //! [Adding a CustomAttribToColorPass pass]

    //! [Adding an image processing pass]
    {
        // pass that draw no object and is positioned at rank 0
        m_imageProcessPass = std::make_shared<RadiumNBR::ImageProcessPass>(3 );
        // set the output of the pass : clear the renderer Linear RGB output texture
        auto vectorFieldTex = renderer->sharedTextures().find( "CustomAtt2Clr::VectorField" );
        m_imageProcessPass->setInputs( *depthtexture, *vectorFieldTex );
        m_imageProcessPass->setOutput( *colortexture );
        // configure access to shader/resources files and initialize the pass
        m_imageProcessPass->setResourcesDir( resourcesPath );
        m_imageProcessPass->initializePass( w, h, shaderManager );
        // add the pass to the renderer and activate it
        renderer->addPass( m_imageProcessPass, m_imageProcessPass->index() );
        m_imageProcessPass->activate();
        // Add the exported (shared) texture to the set of available textures
        auto &sharedTextures = renderer->sharedTextures();
        for (auto t : m_imageProcessPass->getOutputImages() ) {
            sharedTextures.insert( {t.first, t.second} );
        }
    }
    //! [Adding a clear-screen pass]
}

void VisualizationController::update( const Ra::Engine::Data::ViewingParameters& ) {
    /* This function is called once before each frame.
     * You can use it to modify the scene, or modify the computation function of the renderer
     * For this, just call
     * m_customPass->setAttribToColorFunc( customVertexAttrib, customFragmentColor );
     * with the wanted shader glsl source code.
     */
    if ( m_postProcess ) { m_clearPass->setBackground( m_renderer->getBackgroundColor() ); }
    else
    {
        auto defColor = Ra::Core::Utils::Color::linearRGBTosRGB( m_renderer->getBackgroundColor() );
        m_clearPass->setBackground( defColor );
    }
    // TODO : do this only once ?
    if ( m_envmap ) { m_envmap->updateGL(); }
};

void VisualizationController::resize( int w, int h ){
    /* if you have resizeable resources in your controller, this method is called each time the
     * viewer window is resized so that you can dimension your resources accordingly
     */
};

/// Set the custom glsl function for attrib management (vertex) and colorcomputation (fragment)
void VisualizationController::setAttribToColorFunc( const std::string& vertex_source,
                                                    const std::string& geometry_source,
                                                    const std::string& fragment_source ) {
    m_vertexFunction   = vertex_source;
    m_geometryFunction = geometry_source;
    m_fragmentFunction = fragment_source;
    if ( m_customPass )
    {
        m_customPass->setAttribToColorFunc(
            m_vertexFunction, m_geometryFunction, m_fragmentFunction );
        m_customPass->rebuildShaders();
    }
}

std::tuple<std::string&, std::string&, std::string&>
VisualizationController::getAttribToColorFunc() {
    return {m_vertexFunction, m_geometryFunction, m_fragmentFunction};
}

float VisualizationController::getSplatSize() {
    if ( m_customPass )
        return m_customPass->getSplatSize();
    else
    { return 0.025; }
}
void VisualizationController::setSplatSize( float s ) {
    m_customPass->setSplatSize( s );
};


void VisualizationController::setEnvMap( const std::string& files ) {
    if ( files.empty() )
    {
        m_clearPass->setEnvMap( nullptr );
        m_customPass->setEnvMap( nullptr );
        m_hasEnvMap = false;
        m_envmap = nullptr;
    }
    else
    {
        // Todo, add type VERTICALCROSS, HORIZONTALCROSS and differentiate between all types
        //  using file extension and file size
        auto t = ( files.find( ';' ) != files.npos ) ? EnvMap::EnvMapType::ENVMAP_CUBE
                                                     : EnvMap::EnvMapType::ENVMAP_PFM;
        if ( t == EnvMap::EnvMapType::ENVMAP_PFM )
        {
            auto ext = files.substr( files.size() - 3 );
            if ( ext != "pfm" ) { t = EnvMap::EnvMapType::ENVMAP_LATLON; }
        }
        // for now, only skyboxes are managed
        m_envmap = std::make_shared<EnvMap>( files, t, true );
        m_clearPass->setEnvMap( m_envmap );
        m_customPass->setEnvMap( m_envmap );
        m_hasEnvMap = true;
    }
}

void VisualizationController::showEnvMap( bool state ) {
    m_clearPass->showEnvMap( state );
}

void VisualizationController::setEnvStrength( int s ) {
    m_customPass->setEnvStrength( s );
}

void VisualizationController::exportVectorField( bool state ) {
    m_customPass->exportVectorField( state );
}

bool VisualizationController::exportsVectorField() const {
    return m_customPass->exportsVectorField();
}

} // namespace RadiumNBR
