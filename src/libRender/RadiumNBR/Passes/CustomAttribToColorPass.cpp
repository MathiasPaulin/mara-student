#include <RadiumNBR/Passes/CustomAttribToColorPass.hpp>

#ifdef PASSES_LOG
#    include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log
#endif

#include <Engine/Data/Material.hpp>
#include <Engine/Data/ShaderConfigFactory.hpp>
#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/Rendering/RenderObject.hpp>
#include <Engine/Scene/LightManager.hpp>

// TODO for point cloud. Removed from here when it is no more needed.
#include <Engine/Scene/GeometryComponent.hpp>

#include <globjects/Framebuffer.h>

namespace RadiumNBR {
using namespace gl;

// Envmap shader source
static const std::string noopEnvMapFunction{
    "\nint getEnvMapColors(float roughness, vec3 normalWorld, out vec3 diffuse, out vec3 specular) {\n"
    "   diffuse = vec3(1); specular = vec3(1); return 0;\n"
    "}\n\n"
};

static const std::string envMapFunction{
    "\nuniform samplerCube envTexture;\n"
    "uniform int numLod;\n"
    "uniform mat4 redShCoeffs;\n"
    "uniform mat4 greenShCoeffs;\n"
    "uniform mat4 blueShCoeffs;\n"
    "uniform float envStrength;\n"
    "const float OneOverPi = 0.3183098862;\n"
    "int getEnvMapColors(float roughness, vec3 normalWorld, out vec3 diffuse, out vec3 specular) {\n"
    "   vec3 view = normalize(in_viewVector);\n"
    "   vec3 rfl = reflect(-view, normalWorld.xyz);\n"
    "   vec4 up = vec4(normalWorld, 1);\n"
    "   diffuse.r = dot(up, redShCoeffs * up);\n"
    "   diffuse.g = dot(up, greenShCoeffs * up);\n"
    "   diffuse.b = dot(up, blueShCoeffs * up);\n"
    "   diffuse *= envStrength/OneOverPi;\n"
    "   float cosTi = clamp(dot(rfl, normalWorld), 0.001, 1.);\n"
    "   float r = roughness * numLod;\n"
    "   specular = textureLod(envTexture, rfl, r).rgb  * cosTi * envStrength;\n"
    "   return 1;\n"
    "}\n\n"};

/* Test Point cloud parameter provider */
/*
 * WARNING : this class is here only for testing and experimentation purpose.
 * It will be replace soon by a better management of the way components could add specific
 * properties to a rendertechnique
 * TODO : see PR Draft and gist subShaderBlob
 */
class PointCloudParameterProvider : public Ra::Engine::Data::ShaderParameterProvider
{
  public:
    PointCloudParameterProvider( std::shared_ptr<Ra::Engine::Data::Material> mat,
                                 Ra::Engine::Scene::PointCloudComponent* pointCloud ) :
        ShaderParameterProvider(), m_displayMaterial( mat ), m_component( pointCloud ) {}
    ~PointCloudParameterProvider() override = default;
    void updateGL() override {
        m_displayMaterial->updateGL();
        m_renderParameters = m_displayMaterial->getParameters();
        m_renderParameters.addParameter( "pointCloudSplatRadius", m_component->getSplatSize() );
    }

  private:
    std::shared_ptr<Ra::Engine::Data::Material> m_displayMaterial;
    Ra::Engine::Scene::PointCloudComponent* m_component;
};

static const GLenum buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};

CustomAttribToColorPass::CustomAttribToColorPass(
    const std::vector<RenderObjectPtr>* objectsToRender,
    const Ra::Core::Utils::Index& idx ) :
    RenderPass( "Custom AttribToColor pass", idx, objectsToRender ) {}

CustomAttribToColorPass::~CustomAttribToColorPass() = default;

bool CustomAttribToColorPass::initializePass( size_t width,
                                              size_t height,
                                              Ra::Engine::Data::ShaderProgramManager* shaderMngr ) {
    m_shaderMngr = shaderMngr;
    m_fbo        = std::make_unique<globjects::Framebuffer>();

    Ra::Engine::Data::TextureParameters texparams;
    texparams.width          = width;
    texparams.height         = height;
    texparams.target         = GL_TEXTURE_2D;
    texparams.minFilter      = GL_LINEAR;
    texparams.magFilter      = GL_LINEAR;
    texparams.internalFormat = GL_RGBA32F;
    texparams.format         = GL_RGBA;
    texparams.type           = GL_FLOAT;
    texparams.name      = "CustomAtt2Clr::VectorField";
    m_sharedTextures.insert(
        {texparams.name, std::make_shared<Ra::Engine::Data::Texture>( texparams )} );

    return true;
}

void CustomAttribToColorPass::setInputs( const SharedTextures& depthBuffer ) {
    addImportedTextures( {"CustomAtt2Clr::Depth", depthBuffer.second} );
}

void CustomAttribToColorPass::setOutput( const SharedTextures& colorBuffer ) {
    m_outputTexture = colorBuffer;
}

bool CustomAttribToColorPass::update() {

    return true;
}

void CustomAttribToColorPass::resize( size_t width, size_t height ) {

    // Only resize the owned textures. Imported one are resized by their owner
    for ( auto& t : m_localTextures )
    {
        t.second->resize( width, height );
    }

    for ( auto& t : m_sharedTextures )
    {
        t.second->resize( width, height );
    }

    m_fbo->bind();
    m_fbo->attachTexture( GL_DEPTH_ATTACHMENT,
                          m_importedTextures["CustomAtt2Clr::Depth"]->texture() );
    m_fbo->attachTexture( GL_COLOR_ATTACHMENT0, m_outputTexture.second->texture() );
    m_fbo->attachTexture( GL_COLOR_ATTACHMENT1,
                          m_sharedTextures["CustomAtt2Clr::VectorField"]->texture() );
#ifdef PASSES_LOG
    if ( m_fbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    { LOG( logERROR ) << "FBO Error (EmissivityPass::resize): " << m_fbo->checkStatus(); }
#endif
    // finished with fbo, undbind to bind default
    globjects::Framebuffer::unbind();
}

void CustomAttribToColorPass::execute(
    const Ra::Engine::Data::ViewingParameters& viewParams ) const {
    static constexpr const float clearDepth{1.0f};
    static const float clearColor[4] {0.f, 0.f, 0.f, 0.f};
    m_fbo->bind();
    // only draw into 1 or 2 buffers depending on the export of a vector field
    if ( m_exportVectorField ) {
        GL_ASSERT( glDrawBuffers( 2, buffers ) );
        GL_ASSERT( glClearBufferfv( GL_COLOR, 1, clearColor ) );
    } else {
        GL_ASSERT( glDrawBuffers( 1, buffers ) );
    }

    GL_ASSERT( glDepthMask( GL_TRUE ) );
    GL_ASSERT( glColorMask( GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE ) );
    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthFunc( GL_LEQUAL ) );
    GL_ASSERT( glDisable( GL_BLEND ) );

    glClearBufferfv( GL_DEPTH, 0, &clearDepth );

    if ( m_envmap ) {
        m_passParams.addParameter( "envStrength", m_envmap->getEnvStrength() );
        for ( const auto& ro : *m_objectsToRender )
        {
            ro->render( m_passParams, viewParams, passIndex() );
        }
    } else {
        if ( m_lightmanager->count() > 0 )
        {
            for ( size_t i = 0; i < m_lightmanager->count(); ++i )
            {
                Ra::Engine::Data::RenderParameters passParams;
                const auto l = m_lightmanager->getLight( i );
                l->getRenderParameters( passParams );
                for ( const auto& ro : *m_objectsToRender )
                {
                    ro->render( passParams, viewParams, passIndex() );
                }
            }
        }
    }
}

bool CustomAttribToColorPass::buildRenderTechnique(
    const Ra::Engine::Rendering::RenderObject* ro,
    Ra::Engine::Rendering::RenderTechnique& rt ) const {

    auto mat = const_cast<Ra::Engine::Rendering::RenderObject*>( ro )->getMaterial();
    // Volumes are not allowed here
    if ( mat->getMaterialAspect() == Ra::Engine::Data::Material::MaterialAspect::MAT_DENSITY )
    { return false; }

    if ( m_needConfigRebuild )
    {
        Ra::Engine::Data::ShaderConfigurationFactory::removeConfiguration(
            {"CustomAtt2ClrPass::CustomColorProgram" + ro->getName()} );
    }
    if ( auto cfg = Ra::Engine::Data::ShaderConfigurationFactory::getConfiguration(
             {"CustomAtt2ClrPass::CustomColorProgram" + ro->getName()} ) )
    {
        rt.setConfiguration( *cfg, passIndex() );
        std::shared_ptr<Ra::Engine::Data::ShaderParameterProvider> paramProvider = mat;
        auto pointCloudConst =
            dynamic_cast<const Ra::Engine::Scene::PointCloudComponent*>( ro->getComponent() );
        auto pointCloud = const_cast<Ra::Engine::Scene::PointCloudComponent*>( pointCloudConst );
        if ( pointCloud )
        {
            paramProvider = std::make_shared<PointCloudParameterProvider>( mat, pointCloud );
        }
        rt.setParametersProvider( paramProvider, passIndex() );
    }
    else
    {
        // This part is the default configuration for this pass
        // In this shader, the definition of the function void outputCustomAttrib(), along the
        // declaration of input attribs and output result must be appended to the source.
        // e.g.
        // in vec3 attribName;
        // out vec3 frag_attribName;
        // void outputCustomAttrib() {
        //    frag_attribName = attribName;
        // }
        const std::string vertexShaderSource{
            "#include \"TransformStructs.glsl\"\n"
            "#include \"DefaultLight.glsl\"\n"
            "layout (location = 0) in vec3 in_position;\n"
            "layout (location = 1) in vec3 in_normal;\n"
            "layout (location = 2) in vec3 in_texcoord;\n"
            "layout (location = 3) in vec3 in_vertexcolor;\n"
            "#ifndef DONT_USE_INPUT_TANGENT\n"
            "layout( location = 4 ) in vec3 in_tangent;\n"
            "#endif\n"
            "layout (location = 0) out vec3 out_position;\n"
            "layout (location = 1) out vec3 out_normal;\n"
            "layout (location = 2) out vec3 out_texcoord;\n"
            "layout (location = 3) out vec3 out_vertexcolor;\n"
            "#ifndef DONT_USE_INPUT_TANGENT\n"
            "layout( location = 4 ) out vec3 out_tangent;\n"
            "#endif\n"
            "layout (location = 5) out vec3 out_viewVector;\n"
            "layout (location = 6) out vec3 out_lightVector;\n"
            "uniform Transform transform;\n"
            "void outputCustomAttribs();"
            "void main() {\n"
            "mat4 mvp = transform.proj * transform.view * transform.model;\n"
            "gl_Position = mvp * vec4(in_position, 1.0);\n"
            "vec4 pos = transform.model * vec4(in_position, 1.0);\n"
            "pos /= pos.w;\n"
            "vec3 normal = mat3(transform.worldNormal) * in_normal;\n"
            "vec3 tangent = mat3(transform.model) * in_tangent;\n"
            "vec3 eye = -transform.view[3].xyz * mat3(transform.view);\n"
            "out_position    = vec3(pos);\n"
            "out_normal      = normal;\n"
            "out_texcoord    = in_texcoord;\n"
            "out_vertexcolor = in_vertexcolor;\n"
            "#ifndef DONT_USE_INPUT_TANGENT\n"
            "out_tangent     = tangent;\n"
            "#endif\n"
            "out_viewVector  = vec3(eye - out_position);\n"
            "out_lightVector = getLightDirection(light, out_position);\n"
            "outputCustomAttribs();"
            "}\n"};
        // the function computeCustomColor, alongside the declaration of frag_attrib is appended to
        // the source
        const std::string fragmentShadersource{
            "#include \"DefaultLight.glsl\"\n"
            "#include \"VertexAttribInterface.frag.glsl\"\n"
            "layout (location = 5) in vec3 in_viewVector;\n"
            "layout (location = 6) in vec3 in_lightVector;\n"
            "layout (location = 0) out vec4 out_color;\n"
            "#ifdef EXPORT_VECTOR_FIELD\n"
            "layout (location = 1) out vec4 out_vector_field;\n"
            "#endif\n"
            "vec4 computeCustomColor(Material mat, vec3 light_dir, vec3 view_dir, vec3 "
            "normal_world);\n"
            "void main()\n"
            "{\n"
            "vec3 normalWorld   = getWorldSpaceNormal();\n"
            "vec3 tangentWorld  = getWorldSpaceTangent();\n"
            "vec3 binormalWorld = getWorldSpaceBiTangent();\n"
            "vec4 bc = getDiffuseColor(material, getPerVertexTexCoord());\n"
            "if (toDiscard(material, bc)) discard;\n"
            "normalWorld         = getNormal(material, getPerVertexTexCoord(), "
            "normalWorld, tangentWorld, binormalWorld);\n"
            "binormalWorld       = normalize(cross(normalWorld, tangentWorld));\n"
            "tangentWorld        = normalize(cross(binormalWorld, normalWorld));\n"
            "mat3 world2local;\n"
            "world2local[0]  = vec3(tangentWorld.x, binormalWorld.x, normalWorld.x);\n"
            "world2local[1]  = vec3(tangentWorld.y, binormalWorld.y, normalWorld.y);\n"
            "world2local[2]  = vec3(tangentWorld.z, binormalWorld.z, normalWorld.z);\n"
            "vec3 lightDir = normalize(world2local * in_lightVector);\n"
            "vec3 viewDir = normalize(world2local * in_viewVector);\n"
            "out_color = computeCustomColor(material, lightDir, viewDir, normalWorld);\n"
            "}\n"};

        Ra::Engine::Data::ShaderConfiguration theConfig{"CustomAtt2ClrPass::CustomColorProgram" +
                                                        ro->getName()};
        theConfig.addShaderSource( Ra::Engine::Data::ShaderType::ShaderType_VERTEX,
                                   vertexShaderSource + m_customVertexAttrib );
        if ( m_envmap ) {
            theConfig.addShaderSource( Ra::Engine::Data::ShaderType::ShaderType_FRAGMENT,
                                       fragmentShadersource + envMapFunction + m_customFragmentColor );
        } else {
            theConfig.addShaderSource( Ra::Engine::Data::ShaderType::ShaderType_FRAGMENT,
                                       fragmentShadersource + noopEnvMapFunction + m_customFragmentColor );
        }

        if ( m_exportVectorField ) {
            theConfig.addProperty( "EXPORT_VECTOR_FIELD" );
        } else {
            theConfig.removeProperty( "EXPORT_VECTOR_FIELD" );
        }

        theConfig.addInclude( "\"" + mat->getMaterialName() + ".glsl\"",
                              Ra::Engine::Data::ShaderType::ShaderType_FRAGMENT );

        std::shared_ptr<Ra::Engine::Data::ShaderParameterProvider> paramProvider = mat;

        /*
         * WARNING : this way of managing specific geometries is here only for testing and
         * experimentation purpose. It will be replace soon by a better management of the way
         * components could add specific properties to a render technique
         * TODO : see PR Draft and gist subShaderBlob
         */
        const std::string geometryShadersource{
            "#include \"DefaultLight.glsl\"\n"
            "#include \"TransformStructs.glsl\"\n"
            "in gl_PerVertex {\n"
            "    vec4 gl_Position;\n"
            "   float gl_PointSize;\n"
            "    float gl_ClipDistance[];\n"
            "}\n"
            "gl_in[];\n"
            "out gl_PerVertex {\n"
            "    vec4 gl_Position;\n"
            "    float gl_PointSize;\n"
            "    float gl_ClipDistance[];\n"
            "};\n"
            "layout( points ) in;\n"
            "layout( triangle_strip, max_vertices = 4 ) out;\n"
            "layout( location = 0 ) in vec3 in_position[];\n"
            "layout( location = 1 ) in vec3 in_normal[];\n"
            "layout( location = 2 ) in vec3 in_texcoord[];\n"
            "layout( location = 3 ) in vec3 in_vertexColor[];\n"
            "layout( location = 4 ) in vec3 in_tangent[];\n"
            "layout( location = 5 ) in vec3 in_viewVector[];\n"
            "layout( location = 6 ) in vec3 in_lightVector[];\n"
            "uniform Transform transform;\n"
            "uniform float pointCloudSplatRadius;\n"
            "layout( location = 0 ) out vec3 out_position;\n"
            "layout( location = 1 ) out vec3 out_normal;\n"
            "layout( location = 2 ) out vec3 out_texcoord;\n"
            "layout( location = 3 ) out vec3 out_vertexcolor;\n"
            "layout( location = 4 ) out vec3 out_tangent;\n"
            "layout( location = 5 ) out vec3 out_viewVector;\n"
            "layout( location = 6 ) out vec3 out_lightVector;\n"
            "void propagateAttributes();\n"
            "void main() {\n"
            "vec3 eye = -transform.view[3].xyz * mat3( transform.view );\n"
            "// if no normal is provided, splats are aligned with the screen plane\n"
            "vec3 normal = in_normal[0];\n"
            "if ( length( normal ) < 0.1 ) { normal = normalize( eye - in_position[0] ); }\n"
            "// orthonormal basis {u, v, normal}\n"
            "vec3 u = vec3( 1, 0, 0 );\n"
            "if ( abs( normal.x ) > 1e-3 ) { u = normalize( vec3( -normal.z / normal.x, 0, 1 ) ); "
            "}\n"
            "vec3 v = normalize( cross( normal, u ) );\n"
            "vec3 point[4];\n"
            "point[0] = in_position[0] - pointCloudSplatRadius * ( u + v );\n"
            "point[1] = point[0] + pointCloudSplatRadius * u * 2;\n"
            "point[2] = point[0] + pointCloudSplatRadius * v * 2;\n"
            "point[3] = point[0] + pointCloudSplatRadius * ( u + v ) * 2;\n"
            "vec2 uv[4];\n"
            "uv[0] = vec2( -1, -1 );\n"
            "uv[1] = vec2( -1, +1 );\n"
            "uv[2] = vec2( +1, -1 );\n"
            "uv[3] = vec2( +1, +1 );\n"
            "for ( int idx = 0; idx < 4; ++idx ) {\n"
            "    gl_Position     = transform.proj * transform.view * vec4( point[idx], 1 );\n"
            "    out_position    = point[idx];\n"
            "    out_texcoord    = vec3( uv[idx], 0. );\n"
            "    out_normal      = normal;\n"
            "    out_tangent     = length(in_tangent[0]) == 0 ? (point[2] - point[1]) : in_tangent[0];\n"
            "    out_viewVector  = in_viewVector[0];\n"
            "    out_lightVector = in_lightVector[0];\n"
            "    out_vertexcolor = in_vertexColor[0];\n"
            "    propagateAttributes();"
            "    EmitVertex();\n"
            "}\n"
            "EndPrimitive();\n"
            "}\n"};

        auto pointCloudConst =
            dynamic_cast<const Ra::Engine::Scene::PointCloudComponent*>( ro->getComponent() );
        auto pointCloud = const_cast<Ra::Engine::Scene::PointCloudComponent*>( pointCloudConst );
        if ( pointCloud )
        {
            theConfig.addShaderSource( Ra::Engine::Data::ShaderType::ShaderType_GEOMETRY,
                                       geometryShadersource + m_customGeometryAttrib );
            // construct the parameter provider for the technique
            paramProvider = std::make_shared<PointCloudParameterProvider>( mat, pointCloud );
        }
        // Add to the ShaderConfigManager
        Ra::Engine::Data::ShaderConfigurationFactory::addConfiguration( theConfig );
        // Add to the RenderTechnique
        rt.setConfiguration( theConfig, passIndex() );
        rt.setParametersProvider( paramProvider, passIndex() );
    }

    return true;
}

void CustomAttribToColorPass::setAttribToColorFunc( const std::string& vertex_source,
                                                    const std::string& geometry_source,
                                                    const std::string& fragment_source ) {
    m_customVertexAttrib   = vertex_source;
    m_customFragmentColor  = fragment_source;
    m_customGeometryAttrib = geometry_source;
    rebuildShaders();
}

void CustomAttribToColorPass::setLightManager( const Ra::Engine::Scene::LightManager* lm ) {
    m_lightmanager = lm;
}

void CustomAttribToColorPass::rebuildShaders() {
    m_needConfigRebuild = true;
    for ( const auto& ro : *m_objectsToRender )
    {
        auto rt = ro->getRenderTechnique();
        buildRenderTechnique( ro.get(), *rt );
    }
    m_needConfigRebuild = false;
}

void CustomAttribToColorPass::setSplatSize( float s ) {
    m_splatsSize = s;
    for ( auto ro : *m_objectsToRender )
    {
        auto pointCloud =
            dynamic_cast<Ra::Engine::Scene::PointCloudComponent*>( ro->getComponent() );
        if ( pointCloud ) { pointCloud->setSplatSize( m_splatsSize ); }
    }
}

void CustomAttribToColorPass::setEnvMap( std::shared_ptr<EnvMap> envmp ) {
    m_envmap = envmp;
    if ( m_envmap )
    {
        // Set the envmap parameter once.
        // Warning : there is no way to remove parameters. Might segfault if this is not consistent
        // with rendering usage
        m_passParams.addParameter( "redShCoeffs", m_envmap->getShMatrix( 0 ) );
        m_passParams.addParameter( "greenShCoeffs", m_envmap->getShMatrix( 1 ) );
        m_passParams.addParameter( "blueShCoeffs", m_envmap->getShMatrix( 2 ) );
        m_passParams.addParameter( "envTexture", m_envmap->getEnvTexture() );
        int numLod = std::log2(
            std::min( m_envmap->getEnvTexture()->width(), m_envmap->getEnvTexture()->height() ) );
        m_passParams.addParameter( "numLod", numLod );
    }
    rebuildShaders();
}

} // namespace RadiumNBR
