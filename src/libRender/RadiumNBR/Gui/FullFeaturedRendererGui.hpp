#pragma once
#include <RadiumNBR/NodeBasedRendererMacro.hpp>

#include <RadiumNBR/Gui/RendererPanel.hpp>
namespace RadiumNBR {
class FullFeatureRenderer;

NodeBasedRenderer_LIBRARY_API RadiumNBR::Gui::RendererPanel*
buildRadiumNBRGui( FullFeatureRenderer* renderer, const std::function<void()>& appUpdateCallback );

} // namespace RadiumNBR
