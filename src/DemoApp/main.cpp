// Include Radium base application and its simple Gui
#include <Gui/BaseApplication.hpp>
#include <Gui/RadiumWindow/SimpleWindowFactory.hpp>

// To manage the scene
#include <Core/Geometry/MeshPrimitives.hpp>
#include <Engine/Scene/EntityManager.hpp>
#include <Engine/Scene/GeometryComponent.hpp>
#include <Engine/Scene/GeometrySystem.hpp>

// Include the customizable renderer system (only the used part here)
#include <RadiumNBR/Gui/VisualizationGui.hpp>
#include <RadiumNBR/NodeBasedRenderer.hpp>
#include <RadiumNBR/Renderer/Visualization.hpp>

// To add attribs to loaded objects
#include <Engine/Data/Mesh.hpp>
#include <Engine/Rendering/RenderObject.hpp>
#include <Engine/Rendering/RenderObjectManager.hpp>
#include <Engine/Scene/SystemDisplay.hpp>

#include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log

#include <QDockWidget>
#include <QToolBar>
/**
 * This class parameterize the renderer just after the OpenGL system was initialized.
 * when a method of this controller is called, the OpenGL context of the drawing window is
 * activated.
 */
class RendererController : public RadiumNBR::VisualizationController
{
  public:
    void configure( RadiumNBR::NodeBasedRenderer* renderer, int w, int h ) override {
        /*
         * Called once : configure the renderer by adding passes and allocating controller resources
         */
        RadiumNBR::VisualizationController::configure( renderer, w, h );
        /* add here your specific configure step :
         *      -- add more passes, allocate resources, ...
         */
    }

    void update( const Ra::Engine::Data::ViewingParameters& params ) override {
        RadiumNBR::VisualizationController::update( params );
        /* This function is called once before each frame.
         * You can use it to modify the scene, or modify the computation function of the renderer
         * For this, just call
         * m_customPass->setAttribToColorFunc( customVertexAttrib, customFragmentColor );
         * with the wanted shader glsl source code.
         */
    };

    void resize( int w, int h ) override {
        RadiumNBR::VisualizationController::resize( w, h );
        /* if you have resizeable resources in your controller, this method is called each time the
         * viewer window is resized so that you can dimension your resources accordingly
         */
    };
};

/** Process the scene to add the custom attribs to all objects
 */
void AddCustomAttributeToMeshes() {
    auto entityManager = Ra::Engine::RadiumEngine::getInstance()->getEntityManager();
    auto entities      = entityManager->getEntities();
    auto roManager     = Ra::Engine::RadiumEngine::getInstance()->getRenderObjectManager();

    for ( const auto e : entities )
    {
        if ( e == Ra::Engine::Scene::SystemEntity::getInstance() ) { continue; }
        for ( const auto& c : e->getComponents() )
        {
            for ( const auto& roIdx : c->m_renderObjects )
            {
                const auto& ro = roManager->getRenderObject( roIdx );
                if ( ro->getType() == Ra::Engine::Rendering::RenderObjectType::Geometry )
                {
                    auto theMesh = ro->getMesh();

                    auto displayMesh =
                        std::dynamic_pointer_cast<Ra::Engine::Data::AttribArrayDisplayable>(
                            theMesh );

                    LOG( logINFO ) << "\t\tMesh " << displayMesh->getName() << " is processed.";
                    auto& coreMesh = displayMesh->getAttribArrayGeometry();

                    auto addAttrib = [&coreMesh, displayMesh]( const std::string& name,
                                                               auto value ) {
                        coreMesh.addAttrib<Ra::Core::Vector4>( "myCustomAttrib", value );
                    };
                    auto meshTypeHash = typeid( *( theMesh.get() ) ).hash_code();
                    if ( meshTypeHash == typeid( Ra::Engine::Data::Mesh ).hash_code() )
                    {
                        addAttrib( "myCustomAttrib",
                                   Ra::Core::Vector4Array{displayMesh->getNumVertices(),
                                                          {0.6_ra, 0.6_ra, 0.7_ra, 1_ra}} );
                    }
                    else if ( meshTypeHash == typeid( Ra::Engine::Data::PolyMesh ).hash_code() )
                    {
                        addAttrib( "myCustomAttrib",
                                   Ra::Core::Vector4Array{displayMesh->getNumVertices(),
                                                          {0.6_ra, 0.7_ra, 0.6_ra, 1_ra}} );
                    }
                    else if ( meshTypeHash == typeid( Ra::Engine::Data::PointCloud ).hash_code() )
                    {
                        addAttrib( "myCustomAttrib",
                                   Ra::Core::Vector4Array{displayMesh->getNumVertices(),
                                                          {0.7_ra, 0.6_ra, 0.6_ra, 1_ra}} );
                    }
                    else
                    {
                        LOG( logINFO ) << "\t\tMesh " << theMesh->getName()
                                       << " is nor a trianglemesh, nor a polymesh, nor a "
                                          "point cloud ... skipping";
                    }
                }
            }
        }
    }
}

/**
 * The custom color computation shaders
 */

/* Vertex shader : here, the objective is to transform any set ov vertex
 * attribute to any set of fragment shaders input.
 * In this example, we just copy the custom attrib to the output.
 * All standards attribs can be accessed using their Radium name.
 * If needed, all these vectors are expresed in the world frame :
 * out_viewVector, out_lightVector, out_position, out_normal (if available)
 * if available from the underlying geometry, the following parameters can be used :
 * out_texcoord, out_vertexcolor.
 *
 * Note that this string could also be loaded from a file and/or manually edited if you
 * add an edition widget as in Radium app demo
 */
const std::string customVertexAttrib{"in vec4 myCustomAttrib;\n"
                                     "out vec4 geomCustomAttrib;\n"
                                     "\nvoid outputCustomAttribs() { \n"
                                     "geomCustomAttrib=myCustomAttrib;\n"
                                     "}\n"};

/* Geometry shader : just copy the attrib
 *
 */
const std::string customGeomAttrib{"in vec4 geomCustomAttrib[];\n"
                                   "out vec4 fragCustomAttrib;\n"
                                   "\nvoid propagateAttributes() { \n"
                                   "fragCustomAttrib=geomCustomAttrib[0];\n"
                                   "}\n"};

/* Fragment shader : here, the objective is to compute the final color of the fragment.
 * This function get the Material associated with the objet. This material could be
 * used through the Radium GLSL BSDF interface (see Radium doc)
 * All standard interpolated attributes from vertices can be fetched using the Radium
 * GLSL VertexAttribInterface (see Radium doc)
 * The Light could be used through the Radium GLSL light interface.
 *
 * In the following exemple, the lighting is computed using the custom attrib as
 * diffuse reflectance.
 *
 * Note that this string could also be loaded from a file and/or manually edited if you
 * add an edition widget as in Radium app demo
 */
const std::string customFragmentColor{
    "in vec4 fragCustomAttrib;"
    "\nvec4 computeCustomColor(Material mat, vec3 lightDir, vec3 viewDir, vec3 normal_world) { \n"
    "vec3 diffColor; \n"
    "vec3 specColor; \n"
    "getSeparateBSDFComponent( mat, getPerVertexTexCoord(), lightDir,  viewDir,\n"
    "vec3(0, 0, 1), diffColor, specColor );\n"
    "vec3 envd;\n"
    "vec3 envs;\n"
    "float r = getGGXRoughness(mat, getPerVertexTexCoord());\n"
    "int e = getEnvMapColors(r, normal_world, envd, envs);"
    "vec3 finalColor = normal_world*0.5+0.5;\n"
    "finalColor *= fragCustomAttrib.rgb;\n"
    "if (e==1) { finalColor = finalColor*envd + specColor*envs; }\n"
    "else { finalColor = (finalColor + specColor) * max(lightDir.z, 0) \n"
    "       * lightContributionFrom(light, getWorldSpacePosition().xyz); }\n"
    "#ifdef EXPORT_VECTOR_FIELD\n"
    "out_vector_field = vec4(getWorldSpaceTangent(), 1);\n"
    "#endif\n"
    "return vec4( finalColor, 1); \n"
    "}\n"
   };

/**
 *      Main code to demonstrate the use of a self configured node based renderer
 */

/**
 * Define a factory that set the wanted renderer on the window
 */
class DemoWindowFactory : public Ra::Gui::BaseApplication::WindowFactory
{
  public:
    DemoWindowFactory()  = delete;
    ~DemoWindowFactory() = default;
    explicit DemoWindowFactory( std::shared_ptr<RadiumNBR::NodeBasedRenderer> r ) : renderer( r ) {}
    inline Ra::Gui::MainWindowInterface* createMainWindow() const override {
        auto window = new Ra::Gui::SimpleWindow();
        window->addRenderer( renderer->getRendererName(), renderer );
        // Build the gui for the renderer
        auto controlPanel =
            RadiumNBR::buildControllerGui( renderer.get(), [window]() { window->frameUpdate(); } );
        auto dock = new QDockWidget( "Renderer Controller", window );
        dock->setAllowedAreas( Qt::BottomDockWidgetArea );
        dock->setWidget( controlPanel );
        window->addDockWidget( Qt::BottomDockWidgetArea, dock );
        dock->hide();

        auto toolbar = new QToolBar( "Show Controls ", window );
        toolbar->setAllowedAreas( Qt::BottomToolBarArea );
        toolbar->addAction( dock->toggleViewAction() );
        window->addToolBar( Qt::BottomToolBarArea, toolbar );

        return window;
    }

  private:
    std::shared_ptr<RadiumNBR::NodeBasedRenderer> renderer;
};

/**
 * main function.
 */
int main( int argc, char* argv[] ) {
    if ( argc < 2 )
    {
        std::cerr << "usage : " << argv[0] << " -f <scene_file>" << std::endl;
        return 1;
    }
    //! [Instatiating the renderer giving a customization functor]
    RendererController renderControl;
    auto renderer = std::make_shared<RadiumNBR::NodeBasedRenderer>( renderControl );
    renderControl.setAttribToColorFunc( customVertexAttrib, customGeomAttrib, customFragmentColor );
    //! [Instatiating the renderer giving a customization functor]

    //! [Instatiating the application]
    Ra::Gui::BaseApplication app( argc, argv );
    //! [Instatiating the application]

    //! [Initializing the application]
    // The customization functor is called here
    app.initialize( DemoWindowFactory( renderer ) );
    //! [Initializing the application]

    //! [Processing the scene to define custom attributes]
    AddCustomAttributeToMeshes();
    //! [Processing the scene to define custom attributes]

    return app.exec();
}
