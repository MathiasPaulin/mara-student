#pragma once

#include <Gui/BaseApplication.hpp>

#include<optional>

namespace Mara {
class RadiumPlayer : public Ra::Gui::BaseApplication
{
    Q_OBJECT
  public:
    /** Constructors and destructor.
     *  https://en.cppreference.com/w/cpp/language/rule_of_three
     */
    /** @{ */

    RadiumPlayer( int& argc, char** argv );
    RadiumPlayer( const RadiumPlayer& ) = delete;
    RadiumPlayer( RadiumPlayer&& )      = delete;
    RadiumPlayer& operator=( const RadiumPlayer& ) = delete;
    RadiumPlayer& operator=( RadiumPlayer&& ) = delete;

    ~RadiumPlayer() override;
    /**@}*/

    /// initialize the application
    void initialize( const WindowFactory& factory );

    /// Override the Engine openGL initialization slot
    void engineOpenGLInitialize() override;

    /// Override the extension configuration method
    void addApplicationExtension() override;

  public slots:
    void addPluginsDir();
    void addRenderers();
    void roChanged( Ra::Core::Utils::Index roIndex, bool visible );
    // Frame timers ui slots
    void updateFrameStats( const std::vector<Ra::FrameTimerData>& lastFrames );

  private:
    void parseCommandLine();
    void addConnections();

    /// Current file. First indicate that the file (second) must be loaded at startup
    std::optional<QStringList> m_fileOption{std::nullopt};

  private slots:
    /// Allow to pick using a circle
    void toggleCirclePicking( bool on );

    /// Slot for the picking results from the viewer.
    void handlePicking( const Ra::Engine::Rendering::Renderer::PickingResult& pickingResult );
};
} // namespace Mara
