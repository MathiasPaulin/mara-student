#include <RadiumNBR/Passes/DebugPass.hpp>

//#ifdef PASSES_LOG
#include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log
//#endif

#include <Engine/Data/Material.hpp>
#include <Engine/Data/ShaderConfigFactory.hpp>
#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/Data/Texture.hpp>
#include <Engine/Rendering/RenderObject.hpp>

#include <globjects/Framebuffer.h>

namespace RadiumNBR {
using namespace gl;
static const GLenum buffers[] = {GL_COLOR_ATTACHMENT0};
#define DEBUG_PASS_ID 30
DebugPass::DebugPass( const std::vector<RenderObjectPtr>* objectsToRender ) :
    RenderPass( "User Interface pass", DEBUG_PASS_ID, objectsToRender ) {}

DebugPass::~DebugPass() = default;

bool DebugPass::buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                                      Ra::Engine::Rendering::RenderTechnique& rt ) const {
    // This method is never called for DebugObjects.
    // Rendering use the Default Radium pass
    return true;
}

bool DebugPass::initializePass( size_t width,
                                size_t height,
                                Ra::Engine::Data::ShaderProgramManager* shaderMngr ) {
    m_shaderMngr = shaderMngr;
    m_fbo        = std::make_unique<globjects::Framebuffer>();
    return true;
}

bool DebugPass::update() {
    return false;
}

void DebugPass::resize( size_t width, size_t height ) {
    m_fbo->bind();
    m_fbo->attachTexture( GL_DEPTH_ATTACHMENT, m_importedTextures["Debug::Depth"]->texture() );
    // no need to resize the output texture, it belongs to the Ra::Engine::Rendering::Renderer and
    // is already resized
    m_fbo->attachTexture( GL_COLOR_ATTACHMENT0, m_outputTexture->texture() );
#ifdef PASSES_LOG
    if ( m_fbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    { LOG( logERROR ) << "FBO Error (EmissivityPass::resize): " << m_fbo->checkStatus(); }
#endif
    // finished with fbo, undbind to bind default
    globjects::Framebuffer::unbind();
}

void DebugPass::execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const {

    m_fbo->bind();
    glDrawBuffers( 1, buffers );
    GL_ASSERT( glDisable( GL_BLEND ) );
    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthMask( GL_FALSE ) );
    GL_ASSERT( glDepthFunc( GL_LESS ) );

    for ( const auto& ro : *m_objectsToRender )
    {
        ro->render( {}, viewParams );
    }

    //   DebugRender::getInstance()->render( renderData.viewMatrix, renderData.projMatrix );

    m_fbo->unbind();
}

/// Add the output colorBuffer
void DebugPass::setOutput( const Ra::Engine::Data::Texture* colorBuffer ) {
    m_outputTexture = colorBuffer;
}

/// These inputs must be computed before executing this pass
/// depth buffer ? // TODO verify we need this
void DebugPass::setInputs( const SharedTextures& depthBuffer ) {
    addImportedTextures( {"Debug::Depth", depthBuffer.second} );
}

} // namespace RadiumNBR
