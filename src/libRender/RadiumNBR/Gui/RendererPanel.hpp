#pragma once
#include <RadiumNBR/NodeBasedRendererMacro.hpp>

#include <QFrame>
#include <QVBoxLayout>

#include <functional>
#include <stack>
#include <string>

#include <Core/Utils/Color.hpp>

namespace RadiumNBR {
namespace Gui {
/**
 * Renderer specific gui.
 * A renderer panel will expose all configurable options of a renderer to allow the user to
 * interact with the renderer.
 * @todo, allow to open layout to arrange client elements instead of putting them always in the main
 * layout
 */
class NodeBasedRenderer_LIBRARY_API RendererPanel : public QFrame
{
    Q_OBJECT
  public:
    /** Constructors and destructor.
     *  https://en.cppreference.com/w/cpp/language/rule_of_three
     */
    /** @{ */
    explicit RendererPanel( const std::string& name, QWidget* parent = nullptr );
    RendererPanel( const RendererPanel& ) = delete;
    RendererPanel& operator=( const RendererPanel& ) = delete;
    RendererPanel( RendererPanel&& )                 = delete;
    RendererPanel& operator=( RendererPanel&& ) = delete;
    ~RendererPanel() override                   = default;
    /**@}*/

    /**Methods to populate the panel with dedicated ui elements.
     */
    /** @{ */

    /**
     * Open a box layout to organise the widgets
     * @param dir the direction of the layout
     * direction could be :
     *      - QBoxLayout::LeftToRight
     *      - QBoxLayout::RightToLeft
     *      - QBoxLayout::TopToBottom
     *      - QBoxLayout::BottomToTop
     */
    void beginLayout( QBoxLayout::Direction dir = QBoxLayout::LeftToRight );

    /**
     * Close the current layout.
     * When no layout is opened, widgets are arranged vertically into the panel
     */
    void endLayout( bool separator = false );

    /**
     * Add a separator
     */
    void addSeparator();

    /** Add an option to the panel
     *  An option is an on/off checkbox to activate a state of the renderer.
     * @param name The name of the option
     * @param callback The function to call when the state changed
     * @param set The initial value of the option
     */
    void
    addOption( const std::string& name, std::function<void( bool )> callback, bool set = false );

    /** Add a color input button.
     * This button, when pressed, will open a color chooser and call the associated callback.
     * The button is also painted with the choosen color.
     * @param name The name of the color to choose
     * @param callback The function to call when the state changed
     * @param color The initial value of the color
     */
    void addColorInput( const std::string& name,
                        const std::function<void( const Ra::Core::Utils::Color& clr )>& callback,
                        Ra::Core::Utils::Color color = Ra::Core::Utils::Color::Black() );

    /** Add a scalar input spinbox.
     *  This input will return a scalar value within  given bounds.
     * @param name The name of the scalar value
     * @param callback The function to call when the state changed
     * @param initial The initial value of the color
     * @param min The min bound of the value
     * @param max The max bound of the value
     * @param dec The display precision (decimals) of the value
     */
    void addScalarInput( const std::string& name,
                         std::function<void( Scalar )> callback,
                         Scalar initial,
                         Scalar min = 0,
                         Scalar max = 100,
                         int dec    = 3 );

    /** Add an horizontal slider input.
     *  This input will return a scalar value within  given bounds.
     * @param name The name of the scalar value
     * @param callback The function to call when the state changed
     * @param initial The initial value of the color
     * @param min The min bound of the value
     * @param max The max bound of the value
     * @param dec The display precision (decimals) of the value
     */
    void addSliderInput( const std::string& name,
                         std::function<void( int )> callback,
                         int initial = 0,
                         int min     = 0,
                         int max     = 100 );

    /** Add an horizontal power slider input.
     *  This input will return a scalar value within  given bounds.
     * @param name The name of the scalar value
     * @param callback The function to call when the state changed
     * @param initial The initial value of the color
     * @param min The min bound of the value
     * @param max The max bound of the value
     * @param dec The display precision (decimals) of the value
     */
    void addPowerSliderInput( const std::string& name,
                              std::function<void( double )> callback,
                              double initial = 0,
                              double min     = 0,
                              double max     = 100 );

    /** Add a file dialog to the ui.
     * Allow the user to select a file to load according to given filters
     * @param name The name of the file property to expose
     * @param callback The function to call when the state changed
     * @param rootDirectory The initial directory of the file dialog
     * @param filters The filters to apply to filenames
     */
    void addFileInput( const std::string& name,
                       std::function<void( std::string )> callback,
                       const std::string& rootDirectory,
                       const std::string& filters );

    /** Add a save file dialog to the ui.
     * Allow the user to select a file to load according to given filters
     * @param name The name of the file property to expose
     * @param callback The function to call when the state changed
     * @param rootDirectory The initial directory of the file dialog
     * @param filters The filters to apply to filenames
     */
    void addFileOuput( const std::string& name,
                       std::function<void( std::string )> callback,
                       const std::string& rootDirectory,
                       const std::string& filters );
    /**
     *
     * @param name
     * @param callback
     * @param initialText
     */
    void addCodeEditor( const std::string& name,
                        std::function<void( std::string )> callback,
                        std::function<std::string()> initialText );
    /**@}*/

  private:
    /// The layout to organise the ui elements
    QVBoxLayout* m_contentLayout;

    /// The current layout where UI element will be added
    QBoxLayout* m_currentLayout;

    /// The stack of layouts
    std::stack<QBoxLayout*> m_layouts;
};
} // namespace Gui
} // namespace RadiumNBR
