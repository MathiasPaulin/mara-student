#pragma once
#include <RadiumNBR/NodeBasedRendererMacro.hpp>

#include <map>
#include <memory>
#include <string>
#include <vector>

#include <Core/Utils/Index.hpp>

namespace Ra::Engine {
namespace Rendering {
class RenderObject;
class RenderTechnique;
} // namespace Rendering
namespace Data {
class Texture;
class ShaderProgramManager;
struct ViewingParameters;
} // namespace Data
} // namespace Ra::Engine

#include <Engine/Data/RenderParameters.hpp>

namespace RadiumNBR {
/// Interface for RenderPasses
/// @todo Define a protected interface  to access and manipulate common attributes. For now,
/// protected attributes are used but this is a bad design (@see
/// https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#c133-avoid-protected-data)
/// @todo as RenderTechnique has a predefined maximum number of passes, ensure that there will not
/// be more passes than this limit (32 as defined in RenterTechnique.hpp)
class NodeBasedRenderer_LIBRARY_API RenderPass
{
  protected:
    /// Alias for Render Object shared ptr
    using RenderObjectPtr = std::shared_ptr<Ra::Engine::Rendering::RenderObject>;

  public:
    /// Alias for named textures shared between passes
    using SharedTextures = std::pair<std::string, std::shared_ptr<Ra::Engine::Data::Texture>>;

    /// Create a pass and give it a identifier
    /// @param name the name of the pass
    RenderPass( std::string name,
                const Ra::Core::Utils::Index& idx,
                const std::vector<RenderObjectPtr>* objectsToRender ) :
        m_objectsToRender{objectsToRender}, m_passName{std::move( name )}, m_idx{idx} {}
    RenderPass( const RenderPass& ) = delete;
    RenderPass& operator=( const RenderPass& ) = delete;
    RenderPass( RenderPass&& )                 = delete;
    RenderPass& operator=( RenderPass&& ) = delete;

    /// Virtual destructor so that the class could serve as base class
    virtual ~RenderPass() = default;

    /// get the name of the pass
    [[nodiscard]] inline const std::string& getName() const { return m_passName; }

    /// Set the filesystem (real or virtual) location for the pass resources
    inline void setResourcesDir( std::string resourcesRootDir ) {
        resourceDir = std::move( resourcesRootDir );
    }

    /// Get the resources directory
    [[nodiscard]] inline const std::string& getResourcesDir() const { return resourceDir; }

    /// Initialize the renderPass with the given object set
    /// This will compile and link specific shaders, adapted to the objects that will be drawn.
    /// Such shaders will be built assuming that the Ra::Engine::Material associated with the
    /// RendeObjects implements the following Material interface :
    /// A material is a set of functions allowing to describe the appearance of an object and its
    /// constituant microgeometry functions : getNormal, toDiscard, ... global appearance parameters
    /// : getKd (Albedo), getKs ("metalness or associated", ... using this interface accessed by
    /// including the getMaterialName().glsl shader file specific shaders are asssembled to
    /// correspond to the needs of the pass.
    /// @return true if the init was successfull. If not, the pass might be unusable.
    /// @param width the width of the output textures
    /// @param height the height of the output textures
    /// @param The objects that will be rendered
    virtual bool initializePass( size_t width,
                                 size_t height,
                                 Ra::Engine::Data::ShaderProgramManager* shaderMngr ) = 0;

    /// Fill the RenderTechnique to be associated with the renderObject.
    virtual bool buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                                       Ra::Engine::Rendering::RenderTechnique& rt ) const = 0;

    /// Update, if needed, the set of shaders and resources used for this pass.
    /// WARNING : this method is called once per frame on all passes. Keep it efficient
    /// @todo : modify the way RenderObjects are managed in Radium :
    ///      --> do not rebuild render queues from scratch each time, just track the modifications.
    virtual bool update() = 0;

    /// Resize the internal textures and fbo
    virtual void resize( size_t width, size_t height ) = 0;

    /// Get the list of output textures from this pass so that the system can visualize individual
    /// textures by default, returns an empty vector
    [[nodiscard]] std::vector<SharedTextures> getOutputImages() const {
        std::vector<SharedTextures> exported;
        exported.reserve( m_sharedTextures.size() );
        std::copy(
            m_sharedTextures.begin(), m_sharedTextures.end(), std::back_inserter( exported ) );
        return exported;
    }

    /// Add a texture shared by another RenderPass and do not export as output.
    /// The owner is responsible to (eventuelly) export this shared texture as output
    virtual void addImportedTextures( const SharedTextures& texture ) {
        m_importedTextures.insert( texture );
    }

    /// Execute the render pass over the associated objects using the pass internal state and
    /// viewing parameters
    virtual void execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const = 0;

    /// Return the index of the current pass in the renderer
    [[nodiscard]] inline const Ra::Core::Utils::Index& passIndex() const { return m_idx; }

    /// activate the pass
    inline void activate() { m_active = true; }

    /// deactivate the pass
    inline void deactivate() { m_active = false; }

    // is the pass active ?
    [[nodiscard]] inline bool isActive() const { return m_active; }

    [[nodiscard]] inline int index() const { return int( m_idx ); }

    /// @todo : Having protected member is a bad idea
    /// @see https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#c133-avoid-protected-data
  protected:
    /// Textures that are not exposed by the pass but owned by the pass
    std::map<std::string, std::shared_ptr<Ra::Engine::Data::Texture>> m_localTextures;

    /// These are textures imported from other passes (and shared by these).
    std::map<std::string, std::shared_ptr<Ra::Engine::Data::Texture>> m_importedTextures;

    /// Textures that are exposed and shared by the pass
    std::map<std::string, std::shared_ptr<Ra::Engine::Data::Texture>> m_sharedTextures;

    /// Objects to render : this is a reference to a container owned by the renderer that uses the
    /// pass.
    const std::vector<RenderObjectPtr>* m_objectsToRender;

    /// The pass global parameters
    mutable Ra::Engine::Data::RenderParameters m_passParams;

  private:
    /// The name of the pass
    std::string m_passName;
    /// The index of the pass in the RenderTechnique description
    const Ra::Core::Utils::Index m_idx;

    /// The base resources directory
    std::string resourceDir{"./"};

    bool m_active{false};
};
} // namespace RadiumNBR
