#define MARA_PLAYER
#ifdef MARA_PLAYER

#    include <Gui/MainWindow.hpp>
#    include <RadiumPlayer.hpp>

class MainWindowFactory : public Ra::Gui::BaseApplication::WindowFactory
{
  public:
    using Ra::Gui::BaseApplication::WindowFactory::WindowFactory;
    Ra::Gui::MainWindowInterface* createMainWindow() const override {
        return new Mara::MainWindow();
    }
};

int main( int argc, char** argv ) {
    Mara::RadiumPlayer app( argc, argv );
    app.initialize( MainWindowFactory() );
    app.setContinuousUpdate( false );
    return Mara::RadiumPlayer::exec();
}

#else

// Access to the Base Radium application interface
#    include <GuiBase/BaseApplication.hpp>
// Allow to build a Simple Radium window
#    include <GuiBase/RadiumWindow/SimpleWindowFactory.hpp>

/* Includes from the core to define meshes and appearance */
#    include <Core/Asset/BlinnPhongMaterialData.hpp>
#    include <Core/Geometry/MeshPrimitives.hpp>

/* Includes from the Engine to manipulate entities and components*/
#    include <Engine/Component/GeometryComponent.hpp>
#    include <Engine/Managers/EntityManager/EntityManager.hpp>
#    include <Engine/RadiumEngine.hpp>
#    include <Engine/System/GeometrySystem.hpp>

int main( int argc, char** argv ) {
    Ra::GuiBase::BaseApplication app( argc, argv, Ra::GuiBase::SimpleWindowFactory{} );
    app.setContinuousUpdate( false );

    /* Build and add a simple colorized mesh to the engine */
    /* The geometry of the mesh */
    // Warning, the documentation of makeParametricTorus is not so clear about torus radius
    auto torus = Ra::Core::Geometry::makeParametricTorus<128, 32>( 1.5_ra, 0.5_ra );
    // To take the color from vertices but need to be clearer on the Material description
    torus.addAttrib(
        "in_color",
        Ra::Core::Vector4Array{torus.vertices().size(), Ra::Core::Utils::Color::Green()} );
    /* The appearance of the mesh
     * Note that only blinn phong could be used as the TriangleMeshComponent constructor only uses
     * MaterialData from asset
     *  --> Add also others default material in asset ?
     *  --> Put material description in Ra::Core instead of Ra::Engine, to have core materials the
     * same way than core geometry ?
     */
    Ra::Core::Asset::BlinnPhongMaterialData mat;
    mat.m_shininess    = 128_ra;
    mat.m_hasShininess = true;
    mat.m_diffuse      = Ra::Core::Utils::Color::Magenta();
    mat.m_hasDiffuse = true; // <-- need to set that to take the color from the material description

    /* The entity owning the torus */
    auto e = app.m_engine->getEntityManager()->createEntity( "Magenta torus" );

    /* The geometric component of the entity */
    // If nullptr is given instead of &mat, will use the in_color attrib
    auto c = new Ra::Engine::TriangleMeshComponent( "Torus Mesh", e, std::move( torus ), &mat );

    /* Register the entity/component association to the geometry system */
    auto geometrySystem = app.m_engine->getSystem( "GeometrySystem" );
    geometrySystem->addComponent(
        e, c ); // <-- Need to add the (virtual) method addComponent to the class Ra::Engine::System

    /* Tell the viewer a scene was defined so that it compute the scene aabb, build the needed
     * rendertechniques, etc ...
     * --> instead of "postloadfile" a method that do not refer explicitely to a loaded file could
     * be added the the Ra::GuiBase::MainWindowInterface
     */
    app.m_mainWindow->postLoadFile( "Torus" );

    return Ra::GuiBase::BaseApplication::exec();
}
#endif
