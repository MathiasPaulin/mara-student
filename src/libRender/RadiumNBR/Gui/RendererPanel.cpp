#include <RadiumNBR/Gui/RendererPanel.hpp>

#include <RadiumNBR/Gui/GlslEditor.hpp>
#include <RadiumNBR/Gui/PowerSlider.hpp>

#include <QLabel>
#include <QVBoxLayout>

#include <QColorDialog>
#include <QDialog>
#include <QDialogButtonBox>
#include <QDoubleSpinBox>
#include <QFileDialog>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QRadioButton>
#include <QSlider>

namespace RadiumNBR {
namespace Gui {

RendererPanel::RendererPanel( const std::string& name, QWidget* parent ) : QFrame( parent ) {
    setObjectName( name.c_str() );

    m_currentLayout = new QVBoxLayout( this );
    setLayout( m_currentLayout );
    auto panelName = new QLabel( this );
    panelName->setFrameStyle( QFrame::HLine );
    m_currentLayout->addWidget( panelName );
    // addSeparator();
    setVisible( false );
    m_layouts.push( m_currentLayout );
}

// Method to populate the panel
void RendererPanel::addOption( const std::string& name,
                               std::function<void( bool )> callback,
                               bool set ) {
    auto button = new QRadioButton( name.c_str(), this );
    button->setLayoutDirection( Qt::RightToLeft );
    button->setAutoExclusive( false );
    button->setChecked( set );
    m_currentLayout->addWidget( button );
    connect( button, &QRadioButton::toggled, std::move( callback ) );
}

void RendererPanel::addScalarInput( const std::string& name,
                                    std::function<void( Scalar )> callback,
                                    Scalar initial,
                                    Scalar min,
                                    Scalar max,
                                    int dec ) {
    auto inputLayout = new QHBoxLayout();

    auto inputLabel = new QLabel( tr( name.c_str() ), this );
    auto inputField = new QDoubleSpinBox( this );

    inputField->setMinimum( min );
    inputField->setMaximum( max );
    inputField->setDecimals( dec );
    inputField->setValue( initial );

    inputLayout->addWidget( inputLabel );
    inputLayout->addWidget( inputField );
    connect(
        inputField, QOverload<double>::of( &QDoubleSpinBox::valueChanged ), std::move( callback ) );
    m_currentLayout->addLayout( inputLayout );
}

void RendererPanel::addSliderInput( const std::string& name,
                                    std::function<void( int )> callback,
                                    int initial,
                                    int min,
                                    int max ) {
    auto inputLayout = new QHBoxLayout();
    auto inputLabel  = new QLabel( tr( name.c_str() ), this );
    auto inputField  = new QSlider( Qt::Horizontal, this );
    inputField->setValue( initial );
    inputLayout->addWidget( inputLabel );
    inputLayout->addWidget( inputField );
    connect( inputField, &QSlider::valueChanged, std::move( callback ) );
    m_currentLayout->addLayout( inputLayout );
}

void RendererPanel::addPowerSliderInput( const std::string& name,
                                         std::function<void( double )> callback,
                                         double initial,
                                         double min,
                                         double max ) {
    auto inputLayout = new QHBoxLayout();
    auto inputLabel  = new QLabel( tr( name.c_str() ), this );
    auto inputField  = new PowerSlider();
    inputField->setValue( initial );
    inputField->setRange( min, max );
    inputField->setSingleStep( 0.01 );
    inputLayout->addWidget( inputLabel );
    inputLayout->addWidget( inputField );
    connect( inputField, &PowerSlider::valueChanged, std::move( callback ) );
    m_currentLayout->addLayout( inputLayout );
}

void RendererPanel::addColorInput(
    const std::string& name,
    const std::function<void( const Ra::Core::Utils::Color& clr )>& callback,
    Ra::Core::Utils::Color color ) {
    auto button   = new QPushButton( name.c_str(), this );
    auto rgbValue = color.rgb();
    auto clrBttn  = QColor::fromRgb(
        int( rgbValue[0] * 255 ), int( rgbValue[1] * 255 ), int( rgbValue[2] * 255 ) );
    {
        QString qss = QString( "background-color: %1" ).arg( clrBttn.name() );
        button->setStyleSheet( qss );
    }
    auto clrDlg = [callback, clrBttn, button, name]() mutable {
        QColor c = QColorDialog::getColor( clrBttn, button, name.c_str() );
        if ( c.isValid() )
        {
            // update the background coolor of the viewer
            auto bgk = Ra::Core::Utils::Color::sRGBToLinearRGB( Ra::Core::Utils::Color(
                Scalar( c.redF() ), Scalar( c.greenF() ), Scalar( c.blueF() ), Scalar( 0 ) ) );
            callback( bgk );
            clrBttn     = c;
            QString qss = QString( "background-color: %1" ).arg( c.name() );
            button->setStyleSheet( qss );
        }
        return c;
    };
    connect( button, &QPushButton::clicked, clrDlg );
    m_currentLayout->addWidget( button );
}

void RendererPanel::addFileInput( const std::string& name,
                                  std::function<void( std::string )> callback,
                                  const std::string& rootDirectory,
                                  const std::string& filters ) {
    auto button     = new QPushButton( name.c_str(), this );
    auto fileDialog = [this, callback, rootDirectory, filters]() {
        auto fltrs    = QString::fromStdString( filters );
        auto pathList = QFileDialog::getOpenFileNames(
            this, "Open files", QString::fromStdString( rootDirectory ), fltrs );
        if ( !pathList.empty() )
        {
            std::string fileList;
            for ( const auto& file : pathList )
            {
                fileList += file.toStdString() + ";";
            }
            fileList.erase( fileList.size() - 1 );
            callback( fileList );
        }
        else
        { callback( "" ); }
    };
    connect( button, &QPushButton::clicked, fileDialog );
    m_currentLayout->addWidget( button );
}

void RendererPanel::addFileOuput( const std::string& name,
                                  std::function<void( std::string )> callback,
                                  const std::string& rootDirectory,
                                  const std::string& filters ) {
    auto button     = new QPushButton( name.c_str(), this );
    auto fileDialog = [this, callback, rootDirectory, filters]() {
        auto fltrs    = QString::fromStdString( filters );
        auto filename = QFileDialog::getSaveFileName(
            this, "Save file", QString::fromStdString( rootDirectory ), fltrs );
        if ( !filename.isEmpty() ) { callback( filename.toStdString() ); }
    };
    connect( button, &QPushButton::clicked, fileDialog );
    m_currentLayout->addWidget( button );
}

void RendererPanel::beginLayout( QBoxLayout::Direction dir ) {
    m_layouts.push( m_currentLayout );
    m_currentLayout = new QBoxLayout( dir );
}

void RendererPanel::endLayout( bool separator ) {
    m_layouts.top()->addLayout( m_currentLayout );
    m_currentLayout = m_layouts.top();
    if ( separator ) { addSeparator(); }
    m_layouts.pop();
}

void RendererPanel::addSeparator() {
    QFrame* line = new QFrame();
    switch ( m_currentLayout->direction() )
    {
    case QBoxLayout::LeftToRight:
    case QBoxLayout::RightToLeft:
        line->setFrameShape( QFrame::VLine );
        break;
    case QBoxLayout::TopToBottom:
    case QBoxLayout::BottomToTop:
        line->setFrameShape( QFrame::HLine );
        break;
    default:
        line->setFrameShape( QFrame::HLine );
    }
    line->setFrameShadow( QFrame::Sunken );
    m_currentLayout->addWidget( line );
}

void RendererPanel::addCodeEditor( const std::string& name,
                                   std::function<void( std::string )> callback,
                                   std::function<std::string()> initialText ) {
    // @todo : get a full featured code editor
    // cf https://www.codeproject.com/Articles/1139741/QCodeEditor-Widget-for-Qt
    // (code at https://www.codeproject.com/script/Articles/ViewDownloads.aspx?aid=1139741)
    auto button = new QPushButton( name.c_str(), this );

    QDialog* dialog = nullptr;
    auto editDialog = [this, callback, initialText, dialog]() mutable {
        if ( !dialog )
        {
            dialog = new QDialog( this );

            auto textEditor = new GlslEditor( dialog );
            textEditor->setPlainText( QString::fromStdString( initialText() ) );

            auto buttonBox =
                new QDialogButtonBox( QDialogButtonBox::Save | QDialogButtonBox::Close, dialog );

            connect( buttonBox, &QDialogButtonBox::accepted, [callback, textEditor]() {
                callback( textEditor->toPlainText().toStdString() );
            } );
            connect( buttonBox,
                     &QDialogButtonBox::rejected,
                     [callback, &dialog, initialText]() mutable { dialog->hide(); } );
            QVBoxLayout* mainLayout = new QVBoxLayout;
            mainLayout->addWidget( textEditor );
            mainLayout->addWidget( buttonBox );
            dialog->setLayout( mainLayout );
        }
        else
        {
            auto childs     = dialog->children();
            auto textEditor = dynamic_cast<GlslEditor*>( childs.first() );
            if ( textEditor )
            { textEditor->setPlainText( QString::fromStdString( initialText() ) ); }
            else
            {
                std::cout << "ERROR !!! Text editor is not the first child of the dialog"
                          << std::endl;
            }
        }
        dialog->show();
        dialog->raise();
        dialog->activateWindow();
    };
    connect( button, &QPushButton::clicked, editDialog );

    m_currentLayout->addWidget( button );
}

} // namespace Gui
} // namespace RadiumNBR
