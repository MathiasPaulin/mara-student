[![pipeline status](https://gitlab.com/MathiasPaulin/Radium-RenderingApp/badges/master/pipeline.svg)](https://gitlab.com/MathiasPaulin/Radium-RenderingApp/commits/master)

# Radium-RenderingApp

Experimental rendering application build over Radium libraries. 
Used as a sandbox for Radium V2 rendering capabilities.

See [Documentation and discussion](https://mathiaspaulin.gitlab.io/Radium-RenderingApp)
