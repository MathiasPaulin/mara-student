#include "RadiumPlayer.hpp"
#include <Gui/MainWindow.hpp>

#include <Engine/Rendering/ForwardRenderer.hpp>
#include <Engine/Rendering/RenderObject.hpp>
#include <Engine/Rendering/RenderObjectManager.hpp>
#include <Gui/SelectionManager/SelectionManager.hpp>
#include <Gui/Viewer/Viewer.hpp>

#include <VolumeLoader/VolumeLoader.hpp>

#include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // for LOG( logLEVEL )

#include <QCommandLineParser>
#include <QFileDialog>

#include <RadiumNBR/Gui/FullFeaturedRendererGui.hpp>
#include <RadiumNBR/Renderer/FullFeatureRenderer.hpp>

#include <RadiumNBR/Gui/VisualizationGui.hpp>
#include <RadiumNBR/Renderer/Visualization.hpp>
#ifdef WITH_H3D_SUPPORT
#    include <RadiumH3D/h3DLoader.hpp>
#endif

namespace Mara {

RadiumPlayer::RadiumPlayer( int& argc, char** argv ) :
    Ra::Gui::BaseApplication( argc, argv, "RenderApp", "STORM-IRIT" ) {}

void RadiumPlayer::initialize( const WindowFactory& factory ) {
    Ra::Gui::BaseApplication::initialize( factory );

//#define FORCEKEYMAPPINGMAC
#ifdef FORCEKEYMAPPINGMAC
    {
        QSettings settings;
        std::string configFile = Ra::Core::Resources::getRadiumResourcesDir() + "Configs/macos.xml";
        QString kmpcfg( configFile.c_str() );
        settings.setValue( "configs/load", kmpcfg );
        settings.setValue( "keymapping/config", kmpcfg );
        QString path = settings.value( "configs/load", kmpcfg ).toString();
        LOG( logINFO ) << "reading settings from " << settings.fileName().toStdString() << " ("
                       << path.toStdString() << ")";
    }
#endif
#ifdef SHOWSETTINGSFILE
    {
        QSettings settings;
        LOG( logINFO ) << "Reading settings from " << settings.fileName().toStdString();
    }
#endif
    LOG( logINFO ) << "*** My Awesome Rendering Application (MARA) ***";
    parseCommandLine();
    addConnections();
}

RadiumPlayer::~RadiumPlayer() {
    LOG( logINFO ) << "*** End of MARA ***";
}

void RadiumPlayer::parseCommandLine() {
    QCommandLineParser parser;
    parser.setApplicationDescription( "Radium Rendering Application (MARA)" );
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption fileOpt(
        QStringList{"f", "file", "scene"}, "Open a scene file at startup.", "file name", "" );

    parser.addOptions( {fileOpt} );
    parser.process( *this );

    if ( parser.isSet( fileOpt ) ) { m_fileOption = parser.values( fileOpt ); }
}

void RadiumPlayer::toggleCirclePicking( bool on ) {
    auto mainWindow = dynamic_cast<MainWindow*>( m_mainWindow.get() );
    mainWindow->centralWidget()->setMouseTracking( on );
}

void RadiumPlayer::handlePicking(
    const Ra::Engine::Rendering::Renderer::PickingResult& pickingResult ) {
    Ra::Core::Utils::Index roIndex( pickingResult.getRoIdx() );
    Ra::Engine::RadiumEngine* engine = Ra::Engine::RadiumEngine::getInstance();
    auto mainWindow                  = dynamic_cast<MainWindow*>( m_mainWindow.get() );
    if ( roIndex.isValid() )
    {
        auto ro = engine->getRenderObjectManager()->getRenderObject( roIndex );
        if ( ro->getType() != Ra::Engine::Rendering::RenderObjectType::UI )
        {
            Ra::Engine::Scene::Component* comp = ro->getComponent();
            Ra::Engine::Scene::Entity* ent     = comp->getEntity();

            // For now we don't enable group selection.
            mainWindow->getSelectionManager()->setCurrentEntry(
                Ra::Engine::Scene::ItemEntry( ent, comp, roIndex ),
                QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Current );
        }
    }
    else
    { mainWindow->getSelectionManager()->clear(); }
}

void RadiumPlayer::addConnections() {

    auto mainWindow = dynamic_cast<MainWindow*>( m_mainWindow.get() );

    connect( mainWindow, &MainWindow::frameUpdate, this, &RadiumPlayer::askForUpdate );
    connect( mainWindow, &MainWindow::timeFlow, this, &RadiumPlayer::setContinuousUpdate );
    connect( mainWindow, &MainWindow::fileLoading, this, &RadiumPlayer::loadFile );
    connect( mainWindow, &MainWindow::addPluginsDir, this, &RadiumPlayer::addPluginsDir );
    connect(
        mainWindow, &MainWindow::clearPluginsDir, this, &RadiumPlayer::clearPluginDirectories );
    connect( mainWindow, &MainWindow::roChanged, this, &RadiumPlayer::roChanged );

    connect(
        this, &Ra::Gui::BaseApplication::updateFrameStats, this, &RadiumPlayer::updateFrameStats );
    framesCountForStatsChanged( 25 );

    connect(
        m_viewer, &Ra::Gui::Viewer::toggleBrushPicking, this, &RadiumPlayer::toggleCirclePicking );
    connect( m_viewer, &Ra::Gui::Viewer::rightClickPicking, this, &RadiumPlayer::handlePicking );
}

void RadiumPlayer::addPluginsDir() {
    QString dir = QFileDialog::getExistingDirectory( m_mainWindow.get(),
                                                     tr( "Open Plugins Directory" ),
                                                     "",
                                                     QFileDialog::ShowDirsOnly |
                                                         QFileDialog::DontResolveSymlinks );
    addPluginDirectory( dir.toStdString() );
}

void RadiumPlayer::engineOpenGLInitialize() {
    // initialize here the OpenGL part of the engine used by the application
    Ra::Gui::BaseApplication::engineOpenGLInitialize();
    addRenderers();
}

void RadiumPlayer::addRenderers() {
    auto mainWindow = dynamic_cast<MainWindow*>( m_mainWindow.get() );
    // add the default forward renderer
    {
        // The renderer
        auto forwardRenderer = std::make_shared<Ra::Engine::Rendering::ForwardRenderer>();
        // its control panel
        auto controlPanel = new RadiumNBR::Gui::RendererPanel( "Forward Renderer" );
        auto defColor = Ra::Core::Utils::Color::linearRGBTosRGB( m_viewer->getBackgroundColor() );
        auto clrClbck = [this]( const Ra::Core::Utils::Color& clr ) {
            this->m_viewer->setBackgroundColor( clr );
        };
        controlPanel->addColorInput( "Background Color", clrClbck, defColor );

        controlPanel->addOption( " Wireframe rendering", [mainWindow, forwardRenderer]( bool b ) {
            forwardRenderer->enableWireframe( b );
            emit mainWindow->frameUpdate();
        } );
        controlPanel->addOption(
            " Post processing",
            [mainWindow, forwardRenderer]( bool b ) {
                forwardRenderer->enablePostProcess( b );
                emit mainWindow->frameUpdate();
            },
            true );

        // Add the renderer to the ui and to the application
        mainWindow->addRenderer( "Forward Renderer", forwardRenderer, controlPanel );
    }
    // add experimental renderer
    {
        auto myRenderer          = std::make_shared<RadiumNBR::FullFeatureRenderer>();
        std::string rendererName = myRenderer->getRendererName();
        auto controlPanel =
            RadiumNBR::buildRadiumNBRGui( myRenderer.get(), [this]() { this->askForUpdate(); } );
        mainWindow->addRenderer( rendererName, myRenderer, controlPanel );
    }
    // add visualization renderer
    {
        // TODO, this is a memory leak ... Who's owning the controller
        auto renderControl       = new RadiumNBR::VisualizationController;
        auto myRenderer          = std::make_shared<RadiumNBR::NodeBasedRenderer>( *renderControl );
        std::string rendererName = myRenderer->getRendererName();
        auto controlPanel =
            RadiumNBR::buildControllerGui( myRenderer.get(), [this]() { this->askForUpdate(); } );
        mainWindow->addRenderer( rendererName, myRenderer, controlPanel );
    }
}

void RadiumPlayer::addApplicationExtension() {
#ifdef WITH_H3D_SUPPORT
    m_engine->registerFileLoader(
        std::shared_ptr<Ra::Core::Asset::FileLoaderInterface>( new H3D::H3DLoader ) );
#endif
    m_engine->registerFileLoader(
        std::shared_ptr<Ra::Core::Asset::FileLoaderInterface>( new VolumeLoader ) );
}

void RadiumPlayer::roChanged( Ra::Core::Utils::Index roIndex, bool visible ) {
    m_engine->getRenderObjectManager()->getRenderObject( roIndex )->setVisible( visible );
    askForUpdate();
}

void RadiumPlayer::updateFrameStats( const std::vector<Ra::FrameTimerData>& lastFrames ) {

    long renderTime       = 0;
    long frameTime        = 0;
    long frameToFrameTime = 0;

    for ( uint i = 0; i < lastFrames.size(); ++i )
    {
        renderTime += Ra::Core::Utils::getIntervalMicro( lastFrames[i].renderData.renderStart,
                                                         lastFrames[i].renderData.renderEnd );
        frameTime +=
            Ra::Core::Utils::getIntervalMicro( lastFrames[i].frameStart, lastFrames[i].frameEnd );
        if ( i > 0 )
        {
            frameToFrameTime += Ra::Core::Utils::getIntervalMicro( lastFrames[i - 1].frameEnd,
                                                                   lastFrames[i].frameEnd );
        }
    }
    renderTime /= lastFrames.size();
    frameTime /= lastFrames.size();
    long fps  = ( lastFrames.size() - 1 ) * Scalar( 1000000.0 / frameToFrameTime );
    auto mess = QString( "Last frame %1 μs (%2 μs)- (%3 fps)" )
                    .arg( renderTime )
                    .arg( frameTime )
                    .arg( fps );
    m_mainWindow->statusBar()->showMessage( mess );
}
} // namespace Mara
