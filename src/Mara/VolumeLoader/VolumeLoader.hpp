#pragma once

#include <Core/Asset/FileLoaderInterface.hpp>
#include <IO/RaIO.hpp>
namespace Mara {
class VolumeLoader : public Ra::Core::Asset::FileLoaderInterface
{
  public:
    VolumeLoader();

    ~VolumeLoader() override;

    std::vector<std::string> getFileExtensions() const override;
    bool handleFileExtension( const std::string& extension ) const override;
    Ra::Core::Asset::FileData* loadFile( const std::string& filename ) override;
    std::string name() const override;
};
} // namespace Mara
