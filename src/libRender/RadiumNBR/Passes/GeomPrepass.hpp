#pragma once
#include <RadiumNBR/RenderPass.hpp>

namespace Ra::Engine::Data {
class ShaderProgram;
} // namespace Ra::Engine::Data

namespace globjects {
class Framebuffer;
}

namespace RadiumNBR {
/// Render pass that computes only depth-buffer, normal buffer and worldpos buffer
class NodeBasedRenderer_LIBRARY_API GeomPrePass : public RenderPass
{
  public:
    GeomPrePass( const std::vector<RenderObjectPtr>* objectsToRender,
                 const Ra::Core::Utils::Index& idx );
    ~GeomPrePass() override;

    bool buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                               Ra::Engine::Rendering::RenderTechnique& rt ) const override;
    bool initializePass( size_t width,
                         size_t height,
                         Ra::Engine::Data::ShaderProgramManager* shaderMngr ) override;
    bool update() override;
    void resize( size_t width, size_t height ) override;
    void execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const override;

    /// Add the depthexture to be used as depth buffer
    void setOutput( const SharedTextures& depthTexture );

  private:
    /// The framebuffer used to render this pass
    std::unique_ptr<globjects::Framebuffer> m_fbo{nullptr};

    /// The Shader manager to use when building shaders
    Ra::Engine::Data::ShaderProgramManager* m_shaderMngr{nullptr};
};
} // namespace RadiumNBR
