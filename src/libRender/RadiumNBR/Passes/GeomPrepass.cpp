#include <RadiumNBR/Passes/GeomPrepass.hpp>

#ifdef PASSES_LOG
#    include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log
#endif

#include <Engine/Data/Material.hpp>
#include <Engine/Data/ShaderConfigFactory.hpp>
#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/Data/Texture.hpp>
#include <Engine/Rendering/RenderObject.hpp>

#include <globjects/Framebuffer.h>

namespace RadiumNBR {
using namespace gl;

static const GLenum buffers[] = {GL_COLOR_ATTACHMENT0,
                                 GL_COLOR_ATTACHMENT1,
                                 GL_COLOR_ATTACHMENT2,
                                 GL_COLOR_ATTACHMENT3,
                                 GL_COLOR_ATTACHMENT4,
                                 GL_COLOR_ATTACHMENT5,
                                 GL_COLOR_ATTACHMENT6,
                                 GL_COLOR_ATTACHMENT7};

GeomPrePass::GeomPrePass( const std::vector<RenderObjectPtr>* objectsToRender,
                          const Ra::Core::Utils::Index& idx ) :
    RenderPass( "Geometry & Z prepass", idx, objectsToRender ) {}

GeomPrePass::~GeomPrePass() = default;

bool GeomPrePass::initializePass( size_t width,
                                  size_t height,
                                  Ra::Engine::Data::ShaderProgramManager* shaderMngr ) {
    m_shaderMngr = shaderMngr;
    m_fbo        = std::make_unique<globjects::Framebuffer>();

    Ra::Engine::Data::TextureParameters texparams;
    texparams.width          = width;
    texparams.height         = height;
    texparams.target         = GL_TEXTURE_2D;
    texparams.minFilter      = GL_NEAREST;
    texparams.magFilter      = GL_NEAREST;
    texparams.internalFormat = GL_RGBA32F;
    texparams.format         = GL_RGBA;
    texparams.type           = GL_FLOAT;
    texparams.name           = "GeomPrePass::PosInWorld";
    m_sharedTextures.insert(
        {texparams.name, std::make_shared<Ra::Engine::Data::Texture>( texparams )} );

    texparams.minFilter = GL_LINEAR;
    texparams.magFilter = GL_LINEAR;
    texparams.name      = "GeomPrePass::NormalInWorld";
    m_sharedTextures.insert(
        {texparams.name, std::make_shared<Ra::Engine::Data::Texture>( texparams )} );

    return true;
}

void GeomPrePass::setOutput( const SharedTextures& depthTexture ) {
    addImportedTextures( {"GeomPrePass::Depth", depthTexture.second} );
}

bool GeomPrePass::update() {
    return true;
}

void GeomPrePass::resize( size_t width, size_t height ) {
    // Only resize the owned textures. Imported one are resized by their owner
    for ( auto& t : m_localTextures )
    {
        t.second->resize( width, height );
    }
    for ( auto& t : m_sharedTextures )
    {
        t.second->resize( width, height );
    }

    m_fbo->bind();
    m_fbo->attachTexture( GL_DEPTH_ATTACHMENT,
                          m_importedTextures["GeomPrePass::Depth"]->texture() );
    m_fbo->attachTexture( GL_COLOR_ATTACHMENT0,
                          m_sharedTextures["GeomPrePass::PosInWorld"]->texture() );
    m_fbo->attachTexture( GL_COLOR_ATTACHMENT1,
                          m_sharedTextures["GeomPrePass::NormalInWorld"]->texture() );
#ifdef PASSES_LOG
    if ( m_fbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    { LOG( logERROR ) << "FBO Error (GeomPrePass::resize): " << m_fbo->checkStatus(); }
#endif
    // finished with fbo, undbind to bind default
    globjects::Framebuffer::unbind();
}

void GeomPrePass::execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const {
    using ClearColor = Ra::Core::Utils::Color;
    static const float clearDepth{1.0f};

    m_fbo->bind();

    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthMask( GL_TRUE ) );
    GL_ASSERT( glColorMask( GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE ) );
    // only draw into 2 buffers (Normal, Worldpos)
    GL_ASSERT( glDrawBuffers( 2, buffers ) );
    float pblck[4] = {0, 0, 0, 0};
    GL_ASSERT( glClearBufferfv( GL_COLOR, 0, pblck ) ); // Clear World pos
    GL_ASSERT( glClearBufferfv( GL_COLOR, 1, pblck ) ); // Clear Normals
    GL_ASSERT( glClearBufferfv( GL_DEPTH, 0, &clearDepth ) );

    GL_ASSERT( glDepthFunc( GL_LESS ) );
    GL_ASSERT( glDisable( GL_BLEND ) );
    glPolygonOffset( 1.1f, 1.f );
    glEnable( GL_POLYGON_OFFSET_FILL );
    for ( const auto& ro : *m_objectsToRender )
    {
        ro->render( m_passParams, viewParams, passIndex() );
    }

    // Beware of the state left by the pass : Polygon offset must be enable after this for rendering
    // coherency
}

bool GeomPrePass::buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                                        Ra::Engine::Rendering::RenderTechnique& rt ) const {
    std::string resourcesRootDir = getResourcesDir();
    auto mat = const_cast<Ra::Engine::Rendering::RenderObject*>( ro )->getMaterial();
    // Volumes are not used in geomPrepass
    if ( mat->getMaterialAspect() == Ra::Engine::Data::Material::MaterialAspect::MAT_DENSITY )
    { return false; }
    if ( auto cfg = Ra::Engine::Data::ShaderConfigurationFactory::getConfiguration(
             {"GeomPrePass::" + mat->getMaterialName()} ) )
    { rt.setConfiguration( *cfg, passIndex() ); }
    else
    {
        // Build the shader configuration
        Ra::Engine::Data::ShaderConfiguration theConfig{
            {"GeomPrePass::" + mat->getMaterialName()},
            resourcesRootDir + "Shaders/ZPrepass/zprepass.vert.glsl",
            resourcesRootDir + "Shaders/ZPrepass/zprepass.frag.glsl"};
        // add the material interface to the fragment shader
        theConfig.addInclude( "\"" + mat->getMaterialName() + ".glsl\"",
                              Ra::Engine::Data::ShaderType::ShaderType_FRAGMENT );
        // Add to the ShaderConfigManager
        Ra::Engine::Data::ShaderConfigurationFactory::addConfiguration( theConfig );
        // Add to the RenderTechniq
        rt.setConfiguration( theConfig, passIndex() );
    }
    rt.setParametersProvider( mat, passIndex() );
    return true;
}
} // namespace RadiumNBR
