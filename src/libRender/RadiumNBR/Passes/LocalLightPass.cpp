#include <RadiumNBR/Passes/LocalLightPass.hpp>

#ifdef PASSES_LOG
#    include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log
#endif

#include <Engine/Data/Material.hpp>
#include <Engine/Data/ShaderConfigFactory.hpp>
#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/Data/Texture.hpp>
#include <Engine/Rendering/RenderObject.hpp>

#include <Engine/Scene/DefaultLightManager.hpp>

#include <globjects/Framebuffer.h>

namespace RadiumNBR {
using namespace gl;

static const GLenum buffers[] = {GL_COLOR_ATTACHMENT0};

LocalLightPass::LocalLightPass( const std::vector<RenderObjectPtr>* objectsToRender,
                                const Ra::Core::Utils::Index& idx ) :
    RenderPass( "Local lighting", idx, objectsToRender ) {}

LocalLightPass::~LocalLightPass() = default;

bool LocalLightPass::initializePass( size_t /* width */,
                                     size_t /* height */,
                                     Ra::Engine::Data::ShaderProgramManager* shaderMngr ) {
    m_shaderMngr = shaderMngr;
    m_fbo        = std::make_unique<globjects::Framebuffer>();

    return true;
}

void LocalLightPass::setInputs( const SharedTextures& depthBuffer,
                                const SharedTextures& ambientOcclusion ) {
    addImportedTextures( {"LocalLight::Depth", depthBuffer.second} );
    addImportedTextures( {"LocalLight::AmbOcc", ambientOcclusion.second} );
    m_passParams.addParameter( "amb_occ_sampler", ambientOcclusion.second.get() );
}

void LocalLightPass::setOutput( const SharedTextures& colorBuffer ) {
    m_outputTexture = colorBuffer;
}

bool LocalLightPass::update() {
    return true;
}

void LocalLightPass::resize( size_t width, size_t height ) {
    // Only resize the owned textures. Imported one are resized by their owner
    for ( auto& t : m_localTextures )
    {
        t.second->resize( width, height );
    }

    for ( auto& t : m_sharedTextures )
    {
        t.second->resize( width, height );
    }

    m_fbo->bind();
    m_fbo->attachTexture( GL_DEPTH_ATTACHMENT, m_importedTextures["LocalLight::Depth"]->texture() );
    m_fbo->attachTexture( GL_COLOR_ATTACHMENT0, m_outputTexture.second->texture() );
#ifdef PASSES_LOG
    if ( m_fbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    { LOG( logERROR ) << "FBO Error (LocalLightPass::resize): " << m_fbo->checkStatus(); }
#endif
    // finished with fbo, undbind to bind default
    globjects::Framebuffer::unbind();
}

void LocalLightPass::execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const {
    using ClearColor = Ra::Core::Utils::Color;

    m_fbo->bind();
    // only draw into 1 buffers (Color)
    GL_ASSERT( glDrawBuffers( 1, buffers ) );
    GL_ASSERT( glDepthMask( GL_FALSE ) );
    GL_ASSERT( glColorMask( GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE ) );

    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthFunc( GL_EQUAL ) );
    GL_ASSERT( glEnable( GL_BLEND ) );
    GL_ASSERT( glBlendFunc( GL_ONE, GL_ONE ) );

    /// Todo there is huge way to optimize this rendering loop

    if ( m_lightmanager->count() > 0 )
    {
        // for ( const auto& l : m_lights )
        for ( size_t i = 0; i < m_lightmanager->count(); ++i )
        {
            Ra::Engine::Data::RenderParameters passParams;
            passParams.concatParameters( m_passParams );
            const auto l = m_lightmanager->getLight( i );
            l->getRenderParameters( passParams );

            for ( const auto& ro : *m_objectsToRender )
            {
                ro->render( passParams, viewParams, passIndex() );
            }
        }
    }
#ifdef PASSES_LOG
    else
    { LOG( logINFO ) << "Opaque : no light sources, unable to render"; }
#endif
    // Beware of the state left by the pass
    GL_ASSERT( glDisable( GL_BLEND ) );
}

bool LocalLightPass::buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                                           Ra::Engine::Rendering::RenderTechnique& rt ) const {
    auto mat = const_cast<Ra::Engine::Rendering::RenderObject*>( ro )->getMaterial();
    // Volumes are not used in EnvLightPass
    if ( mat->getMaterialAspect() == Ra::Engine::Data::Material::MaterialAspect::MAT_DENSITY )
    { return false; }

    if ( auto cfg = Ra::Engine::Data::ShaderConfigurationFactory::getConfiguration(
             {"LocalLightPass::" + mat->getMaterialName()} ) )
    { rt.setConfiguration( *cfg, passIndex() ); }
    else
    {
        std::string resourcesRootDir = getResourcesDir();
        // Build the shader configuration
        Ra::Engine::Data::ShaderConfiguration theConfig{
            {"LocalLightPass::" + mat->getMaterialName()},
            resourcesRootDir + "Shaders/LocalLightPass/lightpass.vert.glsl",
            resourcesRootDir + "Shaders/LocalLightPass/lightpass.frag.glsl"};
        // add the material interface to the fragment shader
        theConfig.addInclude( "\"" + mat->getMaterialName() + ".glsl\"",
                              Ra::Engine::Data::ShaderType::ShaderType_FRAGMENT );
        // Add to the ShaderConfigManager
        Ra::Engine::Data::ShaderConfigurationFactory::addConfiguration( theConfig );
        // Add to the RenderTechniq
        rt.setConfiguration( theConfig, passIndex() );
    }
    rt.setParametersProvider( mat, passIndex() );
    return true;
}

void LocalLightPass::setLightManager( const Ra::Engine::Scene::LightManager* lm ) {
    m_lightmanager = lm;
}
} // namespace RadiumNBR
