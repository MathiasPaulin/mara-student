//
// Created by Mathias Paulin on 22/09/2020.
//

#include <Core/Asset/FileData.hpp>
#include <RadiumH3D/Model/h3DModel.hpp>
#include <RadiumH3D/h3DLoader.hpp>

namespace H3D {
using namespace Ra::Core::Asset;
using namespace Ra::Core::Utils;

H3DLoader::H3DLoader() = default;

H3DLoader::~H3DLoader() = default;

std::vector<std::string> H3DLoader::getFileExtensions() const {
    return { "*.h3d", "*.H3D" };
}

bool H3DLoader::handleFileExtension( const std::string& extension ) const {
    return ( extension == "h3d" ) || ( extension == "H3D" );
}

FileData* H3DLoader::loadFile( const std::string& filename ) {
    auto fileData = new FileData( filename );
    fileData->setVerbose( false );

    if ( !fileData->isInitialized() )
    {
        delete fileData;
        return nullptr;
    }

    std::clock_t startTime;
    startTime = std::clock();

    fileData->m_geometryData.clear();
    fileData->m_animationData.clear();

    // get the basedir of the document
    std::string baseDir = filename.substr( 0, filename.rfind( '/' ) + 1 );
    if ( baseDir.empty() ) { baseDir = "./"; }

    // convert gltf scenegraph to Filedata ...
    H3DModel h3dModel( fileData, baseDir );

    if ( !h3dModel( filename ) )
    {
        LOG( logERROR ) << "H3DLoader -- Unable to load h3D scene " << filename << ". Aborting";
        delete fileData;
        return nullptr;
    }

    fileData->m_loadingTime = ( std::clock() - startTime ) / Scalar( CLOCKS_PER_SEC );

    if ( fileData->isVerbose() ) { fileData->displayInfo(); }

    fileData->m_processed = true;

    return fileData;
}

std::string H3DLoader::name() const {
    return { "Microsoft H3D Loader." };
}

} // namespace H3D
