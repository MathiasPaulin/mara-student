#pragma once
#include <RadiumNBR/RenderPass.hpp>

#include <Engine/Data/DisplayableObject.hpp>

namespace Ra::Engine::Data {
class ShaderProgram;
} // namespace Ra::Engine::Data

namespace globjects {
class Framebuffer;
}

namespace RadiumNBR {
/// Render pass do 2D processing on input textures to generate a resulting color texture.
class ImageProcessPass : public RenderPass
{
  public:
    explicit ImageProcessPass( const Ra::Core::Utils::Index& idx );
    ~ImageProcessPass() override;

    bool buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                               Ra::Engine::Rendering::RenderTechnique& rt ) const override;
    bool initializePass( size_t width,
                         size_t height,
                         Ra::Engine::Data::ShaderProgramManager* shaderMngr ) override;
    bool update() override;
    void resize( size_t width, size_t height ) override;
    void execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const override;

    /// Add the output colorBuffer.
    /// \warning the output color buffer must be different from the input one
    /// \todo allow to have same input/output color buffer
    void setOutput( const SharedTextures& colorBuffer );

    /// These inputs must be computed before executing this pass : depth buffer
    void setInputs( const SharedTextures& depthBuffer, const SharedTextures& colorBuffer );

    void rebuildShaders();

  private:
    /// The framebuffer used to render this pass
    std::unique_ptr<globjects::Framebuffer> m_fbo{nullptr};

    /// The Shader manager to use when building shaders
    Ra::Engine::Data::ShaderProgramManager* m_shaderMngr{nullptr};

    /// The color texture for output.Stored here for easy access.
    SharedTextures m_outputTexture;

    /// The quad to be drawn for shader invocation
    std::unique_ptr<Ra::Engine::Data::Displayable> m_quadMesh{nullptr};

    /// The shader that process the image (the Radium shader manager has ownership)
    const Ra::Engine::Data::ShaderProgram* m_shader{nullptr};


};
} // namespace RadiumNBR
