#include <RadiumNBR/NodeBasedRenderer.hpp>

#include <Core/Containers/MakeShared.hpp>

#ifdef PASSES_LOG
#    include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log
#endif

#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/Data/Texture.hpp>
#include <Engine/Rendering/RenderObject.hpp>
#include <Engine/Scene/DefaultCameraManager.hpp>
#include <Engine/Scene/DefaultLightManager.hpp>

#include <globjects/Framebuffer.h>

#include <RadiumNBR/Passes/DebugPass.hpp>
#include <RadiumNBR/Passes/UiPass.hpp>

using namespace Ra::Engine;
using namespace Ra::Engine::Scene;
using namespace Ra::Engine::Data;
using namespace Ra::Engine::Rendering;
namespace RadiumNBR {
using namespace gl;

int NodeBasedRendererMagic = 0xFF0F00F0;

static const GLenum buffers[] = {GL_COLOR_ATTACHMENT0};

static NodeBasedRenderer::RenderControlFunctor noOpController;

NodeBasedRenderer::NodeBasedRenderer() : Renderer(), m_controller{noOpController} {}

NodeBasedRenderer::NodeBasedRenderer( NodeBasedRenderer::RenderControlFunctor& controller ) :
    Renderer(), m_controller{controller} {}

NodeBasedRenderer::~NodeBasedRenderer() = default;

bool NodeBasedRenderer::buildRenderTechnique( RenderObject* ro ) const {
    auto rt = Ra::Core::make_shared<RenderTechnique>();
    for ( auto& rp : renderPasses() )
    {
        rp.second->buildRenderTechnique( ro, *rt );
    }
    rt->updateGL();
    ro->setRenderTechnique( rt );
    return true;
}

void NodeBasedRenderer::initResources() {
    // uses several resources from the Radium engine
    auto resourcesRootDir{RadiumEngine::getInstance()->getResourcesDir() + "Shaders/"};

    m_shaderProgramManager->addShaderProgram( {{"Hdr2Ldr"},
                                               resourcesRootDir + "2DShaders/Basic2D.vert.glsl",
                                               resourcesRootDir + "2DShaders/Hdr2Ldr.frag.glsl"} );

    m_postprocessFbo = std::make_unique<globjects::Framebuffer>();

    // NodeBased renderer : only initializes textures that are shared between all the passes or
    // that are used internally on the renderer.
    // Each pass will manage its own textures and export them if they can be shared.
    TextureParameters texparams;
    texparams.width  = m_width;
    texparams.height = m_height;
    texparams.target = GL_TEXTURE_2D;

    // Depth texture
    texparams.minFilter      = GL_NEAREST;
    texparams.magFilter      = GL_NEAREST;
    texparams.internalFormat = GL_DEPTH_COMPONENT24;
    texparams.format         = GL_DEPTH_COMPONENT;
    texparams.type           = GL_UNSIGNED_INT;
    texparams.name           = "Depth (RadiumNBR)";
    m_sharedTextures.insert( {texparams.name, std::make_shared<Texture>( texparams )} );

    // Linear (and HDR) RGBA Color texture
    texparams.internalFormat = GL_RGBA32F;
    texparams.format         = GL_RGBA;
    texparams.type           = GL_FLOAT;
    texparams.minFilter      = GL_LINEAR;
    texparams.magFilter      = GL_LINEAR;
    texparams.name           = "Linear RGB (RadiumNBR)";
    m_sharedTextures.insert( {texparams.name, std::make_shared<Texture>( texparams )} );
}

void NodeBasedRenderer::initializeInternal() {

    // TODO : this must be done only once, see register system ...
    if ( Ra::Engine::RadiumEngine::getInstance()->getSystem( "DefaultCameraManager" ) == nullptr )
    {
        auto cameraManager = new DefaultCameraManager();
        Ra::Engine::RadiumEngine::getInstance()->registerSystem( "DefaultCameraManager",
                                                                 cameraManager );
    }
    auto lmngr = dynamic_cast<DefaultLightManager*>(
        Ra::Engine::RadiumEngine::getInstance()->getSystem( "DefaultLightManager" ) );
    if ( lmngr == nullptr )
    {
        lmngr = new DefaultLightManager();
        Ra::Engine::RadiumEngine::getInstance()->registerSystem( "DefaultLightManager", lmngr );
    }
    m_lightmanagers.push_back( lmngr );

    // Initialize renderer resources
    initResources();
    m_controller.configure( this, m_width, m_height );

    for ( const auto& t : m_sharedTextures )
    {
        m_secondaryTextures.insert( {t.first, t.second.get()} );
    }

    // Todo cache this in an attribute ?
    auto resourcesCheck = Ra::Core::Resources::getResourcesPath(
        reinterpret_cast<void*>( &RadiumNBR::NodeBasedRendererMagic ), {"Resources/RadiumNBR"} );
    if ( !resourcesCheck )
    {
        LOG( Ra::Core::Utils::logERROR ) << "Unable to find resources for NodeBasedRenderer!";
        return;
    }
    auto resourcesPath{*resourcesCheck};

    // TODO, do we really need to setup an index for the two following passes
    // build the Ui pass
    // todo find a way to replace the "31" by another id
    m_uiPass = std::make_unique<UIPass>( &m_uiRenderObjects );
    m_uiPass->setResourcesDir( resourcesPath );
    m_uiPass->setInputs( *( sharedTextures().find( "Depth (RadiumNBR)" ) ) );
    m_uiPass->setOutput( m_fancyTexture.get() );
    m_uiPass->initializePass( m_width, m_height, m_shaderProgramManager );
    // configure the pass
    m_uiPass->deactivate();

    // build the Debug pass
    m_debugPass = std::make_unique<DebugPass>( &m_debugRenderObjects );
    m_debugPass->setResourcesDir( resourcesPath );
    m_debugPass->setInputs( *( sharedTextures().find( "Depth (RadiumNBR)" ) ) );
    m_debugPass->setOutput( m_fancyTexture.get() );
    m_debugPass->initializePass( m_width, m_height, m_shaderProgramManager );
    // configure the pass
    m_debugPass->deactivate();
}

void NodeBasedRenderer::resizeInternal() {

    m_controller.resize( m_width, m_height );

    m_sharedTextures["Depth (RadiumNBR)"]->resize( m_width, m_height );
    m_sharedTextures["Linear RGB (RadiumNBR)"]->resize( m_width, m_height );

    for ( auto& rp : m_renderPasses )
    {
        rp.second->resize( m_width, m_height );
    }

    m_postprocessFbo->bind();
    m_postprocessFbo->attachTexture( GL_COLOR_ATTACHMENT0, m_fancyTexture->texture() );
#ifdef PASSES_LOG
    if ( m_postprocessFbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    {
        LOG( logERROR ) << "FBO Error (AdvancedRenderer::m_postprocessFbo) : "
                        << m_postprocessFbo->checkStatus();
    }
#endif
    // finished with fbo, undbind to bind default
    globjects::Framebuffer::unbind();
    m_uiPass->resize( m_width, m_height );
    m_debugPass->resize( m_width, m_height );
}

void NodeBasedRenderer::renderInternal( const ViewingParameters& renderData ) {
    // Run each pass, in order.
    for ( const auto& rp : m_renderPasses )
    {
        if ( rp.second->isActive() ) { rp.second->execute( renderData ); }
    }
}

// Draw debug stuff, do not overwrite depth map but do depth testing
void NodeBasedRenderer::debugInternal( const ViewingParameters& renderData ) {
    if ( m_debugPass->isActive() ) { m_debugPass->execute( renderData ); }
#if 0
    if ( m_drawDebug )
    {
        const ShaderProgram* shader;

        m_postprocessFbo->bind();
        GL_ASSERT( glDisable( GL_BLEND ) );
        GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
        GL_ASSERT( glDepthMask( GL_FALSE ) );
        GL_ASSERT( glDepthFunc( GL_LESS ) );

        glDrawBuffers( 1, buffers );

        for ( const auto& ro : m_debugRenderObjects )
        {
            ro->render( RenderParameters{}, renderData );
        }

        DebugRender::getInstance()->render( renderData.viewMatrix, renderData.projMatrix );

        m_postprocessFbo->unbind();

        m_uiXrayFbo->bind();
        // Draw X rayed objects always on top of normal objects
        GL_ASSERT( glDepthMask( GL_TRUE ) );
        GL_ASSERT( glClear( GL_DEPTH_BUFFER_BIT ) );
        for ( const auto& ro : m_xrayRenderObjects )
        {
            if ( ro->isVisible() )
            {
                shader = ro->getRenderTechnique()->getShader();

                // bind data
                shader->bind();
                // lighting for Xray : fixed
                shader->setUniform( "light.color", Ra::Core::Utils::Color::Grey( 5.0 ) );
                shader->setUniform( "light.type", Light::LightType::DIRECTIONAL );
                shader->setUniform( "light.directional.direction", Ra::Core::Vector3( 0, -1, 0 ) );

                Ra::Core::Matrix4 M = ro->getTransformAsMatrix();
                shader->setUniform( "transform.proj", renderData.projMatrix );
                shader->setUniform( "transform.view", renderData.viewMatrix );
                shader->setUniform( "transform.model", M );

                ro->getRenderTechnique()->getMaterial()->bind( shader );

                // render
                ro->getMesh()->render();
            }
        }
        m_uiXrayFbo->unbind();
    }
#endif
}

// Draw UI stuff, always drawn on top of everything else + clear ZMask
void NodeBasedRenderer::uiInternal( const ViewingParameters& renderData ) {
    if ( m_uiPass->isActive() ) { m_uiPass->execute( renderData ); }
#if 0
    const ShaderProgram* shader;

    m_uiXrayFbo->bind();
    glDrawBuffers( 1, buffers );
    // Enable z-test
    GL_ASSERT( glDepthMask( GL_TRUE ) );
    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthFunc( GL_LESS ) );
    GL_ASSERT( glClear( GL_DEPTH_BUFFER_BIT ) );
    for ( const auto& ro : m_uiRenderObjects )
    {
        if ( ro->isVisible() )
        {
            shader = ro->getRenderTechnique()->getShader();

            // bind data
            shader->bind();

            Ra::Core::Matrix4 M  = ro->getTransformAsMatrix();
            Ra::Core::Matrix4 MV = renderData.viewMatrix * M;
            Ra::Core::Vector3 V  = MV.block<3, 1>( 0, 3 );
            Scalar d         = V.norm();

            Ra::Core::Matrix4 S    = Ra::Core::Matrix4::Identity();
            S.coeffRef( 0, 0 ) = S.coeffRef( 1, 1 ) = S.coeffRef( 2, 2 ) = d;

            M = M * S;

            shader->setUniform( "transform.proj", renderData.projMatrix );
            shader->setUniform( "transform.view", renderData.viewMatrix );
            shader->setUniform( "transform.model", M );

            ro->getRenderTechnique()->getMaterial()->bind( shader );

            // render
            ro->getMesh()->render();
        }
    }
    m_uiXrayFbo->unbind();
#endif
}

void NodeBasedRenderer::postProcessInternal( const ViewingParameters& /* renderData */ ) {

    m_postprocessFbo->bind();

    GL_ASSERT( glDrawBuffers( 1, buffers ) );

    GL_ASSERT( glDisable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthMask( GL_FALSE ) );

    auto shader = m_postProcessEnabled ? m_shaderProgramManager->getShaderProgram( "Hdr2Ldr" )
                                       : m_shaderProgramManager->getShaderProgram( "DrawScreen" );
    shader->bind();
    shader->setUniform( "screenTexture", m_sharedTextures["Linear RGB (RadiumNBR)"].get(), 0 );
    m_quadMesh->render( shader );

    GL_ASSERT( glDepthMask( GL_TRUE ) );
    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );

    m_postprocessFbo->unbind();
}

void NodeBasedRenderer::updateStepInternal( const ViewingParameters& renderData ) {

    m_controller.update( renderData );

    // Update passes
    // Note that this could be expensive. Find a way to add attributes to renderObjects so
    // that one ca factorizes some evaluations.
    for ( auto& rp : m_renderPasses )
    {
        if ( rp.second->isActive() ) { rp.second->update(); }
    }
}

bool NodeBasedRenderer::addPass( std::shared_ptr<RadiumNBR::RenderPass> pass,
                                 int rank,
                                 bool defaultPass ) {
    const auto [itPass, success] = m_renderPasses.insert( {rank, pass} );
    if ( defaultPass && success ) { m_defaultPass = rank; }
    return success;
}

void NodeBasedRenderer::showUI( bool b ) {
    m_showUi = b;
    if ( m_showUi ) { m_uiPass->activate(); }
    else
    { m_uiPass->deactivate(); }
}

void NodeBasedRenderer::showDebug( bool b ) {
    m_showDebug = b;
    if ( m_showDebug ) { m_debugPass->activate(); }
    else
    { m_debugPass->deactivate(); }
}

} // namespace RadiumNBR
