#pragma once
#include <RadiumNBR/NodeBasedRendererMacro.hpp>

//
//  Included headers
//
#include <QWidget>
#include <RadiumNBR/Gui/CodeEditor/CodeEditor.hpp>

namespace RadiumNBR::Gui {

///
///  @file      QCodeEditorLineWidget.hpp
///  @author    Nicolas Kogler
///  @date      October 6th, 2016
///  @class     QCodeEditorLineWidget
///  @brief     Paints the line column and the line numbers.
///
class NodeBasedRenderer_LIBRARY_API QCodeEditorLineWidget : public QWidget
{
  public:
    ///
    ///  @fn    Constructor
    ///  @brief Initializes a new instance of QCodeEditorLineWidget.
    ///
    QCodeEditorLineWidget( QCodeEditor* parent );

    ///
    ///  @fn    Destructor
    ///  @brief Frees all resources allocated by QCodeEditorLineWidget.
    ///
    ~QCodeEditorLineWidget();

    ///
    ///  @fn      sizeHint
    ///  @brief   Reserves space for the line widget.
    ///  @returns the desired size.
    ///
    QSize sizeHint() const Q_DECL_OVERRIDE;

  protected:
    ///
    ///  @fn    paintEvent
    ///  @brief Paints the line column and the line numbers.
    ///  @param event Pointer to a QPaintEvent instance
    ///
    void paintEvent( QPaintEvent* event ) Q_DECL_OVERRIDE;

  private:
    //
    // Private class members
    //
    QCodeEditor* m_Parent;
    QWidget* m_EditorView;
};
} // namespace RadiumNBR::Gui
