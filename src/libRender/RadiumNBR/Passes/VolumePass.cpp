#include <RadiumNBR/Passes/VolumePass.hpp>

#include <Core/Utils/Color.hpp>

#ifdef PASSES_LOG
#    include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log
#endif

#include <Core/Geometry/MeshPrimitives.hpp>
#include <Engine/Data/Material.hpp>
#include <Engine/Data/Mesh.hpp>
#include <Engine/Data/ShaderConfigFactory.hpp>
#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/Data/Texture.hpp>
#include <Engine/Rendering/RenderObject.hpp>
#include <Engine/Scene/DefaultLightManager.hpp>

#include <globjects/Framebuffer.h>
namespace RadiumNBR {
using namespace gl;

static const GLenum buffers[] = {GL_COLOR_ATTACHMENT0};

VolumeLightingPass::VolumeLightingPass( const std::vector<RenderObjectPtr>* objectsToRender,
                                        const Ra::Core::Utils::Index& idx ) :
    RenderPass( "Volume lighting", idx, objectsToRender ) {}

VolumeLightingPass::~VolumeLightingPass() = default;

bool VolumeLightingPass::initializePass( size_t width,
                                         size_t height,
                                         Ra::Engine::Data::ShaderProgramManager* shaderMngr ) {
    m_shaderMngr = shaderMngr;
    m_fbo        = std::make_unique<globjects::Framebuffer>();
    m_volumeFbo  = std::make_unique<globjects::Framebuffer>();

    Ra::Engine::Data::TextureParameters texparams;
    texparams.width          = width;
    texparams.height         = height;
    texparams.target         = GL_TEXTURE_2D;
    texparams.minFilter      = GL_LINEAR;
    texparams.magFilter      = GL_LINEAR;
    texparams.internalFormat = GL_RGBA32F;
    texparams.format         = GL_RGBA;
    texparams.type           = GL_FLOAT;
    texparams.name           = "VolumeLighting::RawVolume";
    m_sharedTextures.insert(
        {texparams.name, std::make_shared<Ra::Engine::Data::Texture>( texparams )} );

    // The shader caller
    Ra::Core::Geometry::TriangleMesh mesh =
        Ra::Core::Geometry::makeZNormalQuad( Ra::Core::Vector2( -1.f, 1.f ) );
    auto qm = std::make_unique<Ra::Engine::Data::Mesh>( "caller" );
    qm->loadGeometry( std::move( mesh ) );
    m_quadMesh = std::move( qm );
    m_quadMesh->updateGL();

    // The shader
    const std::string vrtxSrc{"layout (location = 0) in vec3 in_position;\n"
                              "out vec2 varTexcoord;\n"
                              "void main()\n"
                              "{\n"
                              "  gl_Position = vec4(in_position, 1.0);\n"
                              "  varTexcoord = (in_position.xy + 1.0) / 2.0;\n"
                              "}\n"};
    const std::string frgSrc{
        "out vec4 fragColor;\n"
        "in vec2 varTexcoord;\n"
        "uniform sampler2D volumeImage;\n"
        "void main()\n"
        "{\n"
        "  vec2 size = vec2(textureSize(volumeImage, 0));\n"
        "  vec4 volColor = texelFetch(volumeImage, ivec2(varTexcoord.xy * size), 0);\n"
        "  if (volColor.a < 1)\n"
        "    discard;\n"
        "  fragColor = vec4(volColor.rgb, 0);\n"
        "}\n"};
    Ra::Engine::Data::ShaderConfiguration config{"ComposeVolume"};
    config.addShaderSource( Ra::Engine::Data::ShaderType::ShaderType_VERTEX, vrtxSrc );
    config.addShaderSource( Ra::Engine::Data::ShaderType::ShaderType_FRAGMENT, frgSrc );
    auto added = shaderMngr->addShaderProgram( config );
    if ( added ) { m_shader = added.value(); }
    else
    { return false; }

    return true;
}

void VolumeLightingPass::setInputs( const SharedTextures& depthBuffer,
                                    const SharedTextures& colorBuffer ) {
    addImportedTextures( {"VolumeLighting::Depth", depthBuffer.second} );
    addImportedTextures( {"VolumeLighting::ColorInput", colorBuffer.second} );
}

void VolumeLightingPass::setOutput( const SharedTextures& colorBuffer ) {
    m_outputTexture = colorBuffer;
}

bool VolumeLightingPass::update() {
    return true;
}

void VolumeLightingPass::resize( size_t width, size_t height ) {
    // Only resize the owned textures. Imported one are resized by their owner
    for ( auto& t : m_localTextures )
    {
        t.second->resize( width, height );
    }

    for ( auto& t : m_sharedTextures )
    {
        t.second->resize( width, height );
    }

    m_fbo->bind();
    m_fbo->attachTexture( GL_COLOR_ATTACHMENT0, m_outputTexture.second->texture() );
#ifdef PASSES_LOG
    if ( m_fbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    { LOG( logERROR ) << "FBO Error (VolumeLightingPass::mfbo): " << m_fbo->checkStatus(); }
#endif
    m_volumeFbo->bind();
    m_volumeFbo->attachTexture( GL_DEPTH_ATTACHMENT,
                                m_importedTextures["VolumeLighting::Depth"]->texture() );
    m_volumeFbo->attachTexture( GL_COLOR_ATTACHMENT0,
                                m_sharedTextures["VolumeLighting::RawVolume"]->texture() );
#ifdef PASSES_LOG
    if ( m_volumeFbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    {
        LOG( logERROR ) << "FBO Error (VolumeLightingPass::m_volumeFbo) : "
                        << m_volumeFbo->checkStatus();
    }
#endif
    // finished with fbo, undbind to bind default
    globjects::Framebuffer::unbind();
}

void VolumeLightingPass::execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const {
    if ( m_objectsToRender->empty() ) { return; }

    auto colorTex = m_importedTextures.find( "VolumeLighting::ColorInput" );
    auto depthTex = m_importedTextures.find( "VolumeLighting::Depth" );
    auto volTex   = m_sharedTextures.find( "VolumeLighting::RawVolume" );

    m_volumeFbo->bind();
    GL_ASSERT( glDrawBuffers( 1, buffers ) );
    GL_ASSERT( glClearBufferfv( GL_COLOR, 0, Ra::Core::Utils::Color::Alpha().data() ) );
    GL_ASSERT( glDisable( GL_BLEND ) );
    GL_ASSERT( glDepthFunc( GL_LEQUAL ) );
    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthMask( GL_FALSE ) );
    for ( size_t i = 0; i < m_lightmanager->count(); ++i )
    {
        Ra::Engine::Data::RenderParameters passParams;

        passParams.addParameter( "imageColor", colorTex->second.get() );
        passParams.addParameter( "imageDepth", depthTex->second.get() );

        const auto l = m_lightmanager->getLight( i );
        l->getRenderParameters( passParams );

        for ( const auto& ro : *m_objectsToRender )
        {
            ro->render( passParams, viewParams, passIndex() );
        }
    }
    m_fbo->bind();
    GL_ASSERT( glDrawBuffers( 1, buffers ) );
    GL_ASSERT( glDisable( GL_DEPTH_TEST ) );
    GL_ASSERT( glEnable( GL_BLEND ) );
    GL_ASSERT( glBlendFunc( GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA ) );
    {
        m_shader->bind();
        m_shader->setUniform( "volumeImage", volTex->second.get(), 0 );
        m_quadMesh->render( m_shader );
    }
    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDisable( GL_BLEND ) );
}

bool VolumeLightingPass::buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                                               Ra::Engine::Rendering::RenderTechnique& rt ) const {
    std::string resourcesRootDir = getResourcesDir();
    auto mat = const_cast<Ra::Engine::Rendering::RenderObject*>( ro )->getMaterial();
    // Only volumes are used by this pass
    if ( mat->getMaterialAspect() != Ra::Engine::Data::Material::MaterialAspect::MAT_DENSITY )
    { return false; }
    if ( auto cfg = Ra::Engine::Data::ShaderConfigurationFactory::getConfiguration(
             {"VolumeLightingPass::" + mat->getMaterialName()} ) )
    { rt.setConfiguration( *cfg, passIndex() ); }
    else
    {
        // Build the shader configuration
        Ra::Engine::Data::ShaderConfiguration theConfig{
            {"VolumeLightingPass::" + mat->getMaterialName()},
            resourcesRootDir + "Shaders/VolumeLightingPass/volume.vert.glsl",
            resourcesRootDir + "Shaders/VolumeLightingPass/volume.frag.glsl"};
        /*
        // add the material interface to the fragment shader
        theConfig.addInclude( "\"" + mat->getMaterialName() + ".glsl\"",
                              Ra::Engine::ShaderType::ShaderType_FRAGMENT );
        */
        // Add to the ShaderConfigManager
        Ra::Engine::Data::ShaderConfigurationFactory::addConfiguration( theConfig );
        // Add to the RenderTechniq
        rt.setConfiguration( theConfig, passIndex() );
    }
    rt.setParametersProvider( mat, passIndex() );

    return true;
}

void VolumeLightingPass::setLightManager( const Ra::Engine::Scene::LightManager* lm ) {
    m_lightmanager = lm;
}

void VolumeLightingPass::setEnvMap( std::shared_ptr<EnvMap> envmp ) {
    m_envmap = envmp;
}
} // namespace RadiumNBR
