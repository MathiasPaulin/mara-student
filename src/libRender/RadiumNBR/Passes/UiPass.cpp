#include <RadiumNBR/Passes/UiPass.hpp>

//#ifdef PASSES_LOG
#include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log
//#endif

#include <Engine/Data/DisplayableObject.hpp>
#include <Engine/Data/Material.hpp>
#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/Data/Texture.hpp>
#include <Engine/Data/ViewingParameters.hpp>
#include <Engine/Rendering/RenderObject.hpp>

#include <globjects/Framebuffer.h>

namespace RadiumNBR {
using namespace gl;
static const GLenum buffers[] = {GL_COLOR_ATTACHMENT0};
#define UI_PASS_ID 31
UIPass::UIPass( const std::vector<RenderObjectPtr>* objectsToRender) :
    RenderPass( "User Interface pass", UI_PASS_ID, objectsToRender ) {}

UIPass::~UIPass() = default;

bool UIPass::buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                                   Ra::Engine::Rendering::RenderTechnique& rt ) const {
    // This method is never called for UiObjects.
    // Rendering use the Default Radium pass
    return true;
}

bool UIPass::initializePass( size_t width,
                             size_t height,
                             Ra::Engine::Data::ShaderProgramManager* shaderMngr ) {
    m_shaderMngr = shaderMngr;
    m_fbo        = std::make_unique<globjects::Framebuffer>();
    return true;
}

bool UIPass::update() {
    return false;
}

void UIPass::resize( size_t width, size_t height ) {
    m_fbo->bind();
    m_fbo->attachTexture( GL_DEPTH_ATTACHMENT, m_importedTextures["Ui::Depth"]->texture() );
    // no need to resize the output texture, it belongs to the Ra::Engine::Rendering::Renderer and
    // is already resized
    m_fbo->attachTexture( GL_COLOR_ATTACHMENT0, m_outputTexture->texture() );
#ifdef PASSES_LOG
    if ( m_fbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    { LOG( logERROR ) << "FBO Error (EmissivityPass::resize): " << m_fbo->checkStatus(); }
#endif
    // finished with fbo, undbind to bind default
    globjects::Framebuffer::unbind();
}

void UIPass::execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const {
    /*
    LOG( Ra::Core::Utils::logINFO )
        << " requested to render " << m_objectsToRender->size() << " UI objects";
    */
    m_fbo->bind();
    glDrawBuffers( 1, buffers );
    GL_ASSERT( glDepthMask( GL_TRUE ) );
    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthFunc( GL_LESS ) );
    GL_ASSERT( glClear( GL_DEPTH_BUFFER_BIT ) );

    for ( const auto& ro : *m_objectsToRender )
    {
        if ( ro->isVisible() )
        {
            auto shader = ro->getRenderTechnique()->getShader();

            // bind data
            shader->bind();

            Ra::Core::Matrix4 M  = ro->getTransformAsMatrix();
            Ra::Core::Matrix4 MV = viewParams.viewMatrix * M;
            Ra::Core::Vector3 V  = MV.block<3, 1>( 0, 3 );
            Scalar d         = V.norm();

            Ra::Core::Matrix4 S    = Ra::Core::Matrix4::Identity();
            S.coeffRef( 0, 0 ) = S.coeffRef( 1, 1 ) = S.coeffRef( 2, 2 ) = d;

            M = M * S;

            shader->setUniform( "transform.proj", viewParams.projMatrix );
            shader->setUniform( "transform.view", viewParams.viewMatrix );
            shader->setUniform( "transform.model", M );

            auto shaderParameter = ro->getRenderTechnique()->getParametersProvider();
            if ( shaderParameter != nullptr ) shaderParameter->getParameters().bind( shader );

            // render
            ro->getMesh()->render( shader );
        }
    }

    //   DebugRender::getInstance()->render( renderData.viewMatrix, renderData.projMatrix );

    m_fbo->unbind();

}

/// Add the output colorBuffer
void UIPass::setOutput( const Ra::Engine::Data::Texture* colorBuffer ) {
    m_outputTexture = colorBuffer;
}

/// These inputs must be computed before executing this pass
/// depth buffer ? // TODO verify we need this
void UIPass::setInputs( const SharedTextures& depthBuffer ) {
    addImportedTextures( {"Ui::Depth", depthBuffer.second} );
}

} // namespace RadiumNBR
