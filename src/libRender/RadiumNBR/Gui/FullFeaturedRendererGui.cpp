#include <RadiumNBR/Gui/FullFeaturedRendererGui.hpp>
#include <RadiumNBR/Renderer/FullFeatureRenderer.hpp>

namespace RadiumNBR {
using namespace Gui;

RadiumNBR::Gui::RendererPanel* buildRadiumNBRGui( FullFeatureRenderer* renderer,
                                                  const std::function<void()>& appUpdateCallback ) {
    auto controlPanel = new RendererPanel( renderer->getRendererName() );

    auto defColor = Ra::Core::Utils::Color::linearRGBTosRGB( renderer->getBackgroundColor() );
    auto clrClbck = [renderer, appUpdateCallback]( const Ra::Core::Utils::Color& clr ) {
        // set the background color for all passes that need it
        renderer->setBackgroundColor( clr );
        appUpdateCallback();
    };
    controlPanel->addColorInput( "Background Color", clrClbck, defColor );

    controlPanel->addSeparator();

    controlPanel->beginLayout( QBoxLayout::LeftToRight );

    controlPanel->addOption( " Wireframe rendering", [renderer, appUpdateCallback]( bool b ) {
        renderer->wireframeMode( b );
        appUpdateCallback();
    } );

    controlPanel->addOption(
        " Show Debug ",
        [renderer, appUpdateCallback]( bool b ) {
            renderer->showDebug( b );
            appUpdateCallback();
        },
        false );

    controlPanel->addOption(
        " Show UI ",
        [renderer, appUpdateCallback]( bool b ) {
            renderer->showUI( b );
            appUpdateCallback();
        },
        false );
    controlPanel->endLayout( true );

    auto envmpClbck = [renderer, appUpdateCallback]( const std::string& files ) {
        renderer->setEnvMap( files );
        appUpdateCallback();
    };

    controlPanel->beginLayout( QBoxLayout::LeftToRight );
    controlPanel->addFileInput(
        "Environment map", envmpClbck, "../", "Images (*.png *.jpg *.pfm *.exr *hdr)" );

    controlPanel->beginLayout( QBoxLayout::TopToBottom );
    controlPanel->addOption(
        " Show envMap ",
        [renderer, appUpdateCallback]( bool b ) {
            renderer->showEnvMap( b );
            appUpdateCallback();
        },
        true );

    controlPanel->addSliderInput(
        " Environment strength ",
        [renderer, appUpdateCallback]( int v ) {
            renderer->setEnvStrength( v );
            appUpdateCallback();
        },
        100 );
    controlPanel->endLayout();
    controlPanel->endLayout( true );

    // AO control
    controlPanel->beginLayout( QBoxLayout::LeftToRight );
    controlPanel->addScalarInput(
        "AO radius",
        [renderer, appUpdateCallback]( double r ) {
            renderer->setAoRadius( Scalar( r ) );
            appUpdateCallback();
        },
        renderer->getAoRadius(),
        0,
        100,
        2 );

    controlPanel->addScalarInput(
        "AO sampling",
        [renderer, appUpdateCallback]( double r ) {
            renderer->setAoSamplingDensity( int( std::floor( r ) ) );
            appUpdateCallback();
        },
        renderer->getAoSamplingDensity(),
        0,
        4096,
        0 );
    controlPanel->endLayout();

    return controlPanel;
}

} // namespace RadiumNBR
