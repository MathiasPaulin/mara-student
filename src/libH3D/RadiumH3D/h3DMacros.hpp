#pragma once

#include <Core/RaCore.hpp>
/// Defines the correct macro to export dll symbols.
#if defined RadiumH3D_EXPORTS
#    define h3D_LIBRARY_API DLL_EXPORT
#else
#    define h3D_LIBRARY_API DLL_IMPORT
#endif
