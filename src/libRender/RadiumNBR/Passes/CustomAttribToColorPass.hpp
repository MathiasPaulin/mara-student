#pragma once
#include <RadiumNBR/RenderPass.hpp>
#include <RadiumNBR/EnvMap.hpp>

#include <Core/Utils/Color.hpp>

namespace Ra::Engine::Scene {
class LightManager;
}

namespace Ra::Engine::Data {
class ShaderProgram;
} // namespace Ra::Engine::Data

namespace globjects {
class Framebuffer;
}

namespace RadiumNBR {
/// Render pass that draws objects using a custom color computation function (in glsl)
/// For now, this pass is dedicated to visualize only point cloud (and only one at a time here)
/// and can't be mixed with any other pass except clear pass.
class CustomAttribToColorPass : public RenderPass
{
  public:
    CustomAttribToColorPass( const std::vector<RenderObjectPtr>* objectsToRender,
                             const Ra::Core::Utils::Index& idx );
    ~CustomAttribToColorPass() override;

    bool buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                               Ra::Engine::Rendering::RenderTechnique& rt ) const override;
    bool initializePass( size_t width,
                         size_t height,
                         Ra::Engine::Data::ShaderProgramManager* shaderMngr ) override;
    bool update() override;
    void resize( size_t width, size_t height ) override;
    void execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const override;

    /// Add the output colorBuffer
    void setOutput( const SharedTextures& colorBuffer );

    /// These inputs must be computed before executing this pass : depth buffer
    void setInputs( const SharedTextures& depthBuffer );

    void setLightManager( const Ra::Engine::Scene::LightManager* lm );

    /// Set the custom glsl function for attrib management (vertex) and colorcomputation (fragment)
    void setAttribToColorFunc( const std::string& vertex_source,
                               const std::string& geometry_source,
                               const std::string& fragment_source );

    void rebuildShaders();

    /// Set the splat size for pointclouds
    inline float getSplatSize() { return m_splatsSize; }

    /// return the splatSize used for point clouds
    void setSplatSize(float s);

    /// set the env map to use
    void setEnvMap( std::shared_ptr<EnvMap> envmp );

    /// set the strength (aka "power") of the env map
    void setEnvStrength( int s ) {
        if ( m_envmap )
        {
            m_envStrength = float( s ) / 100.f;
            m_envmap->setEnvStrength( m_envStrength );
        }
    }

    /// get the strength (aka "power") of the env map
    int getEnvStrength() const { return int( m_envStrength * 100 ); }

    /// get/set the export vector field state
    void exportVectorField( bool state ) {
        m_exportVectorField = state;
        rebuildShaders();
    };
    bool exportsVectorField() const { return m_exportVectorField;}

  private:
    /// The framebuffer used to render this pass
    std::unique_ptr<globjects::Framebuffer> m_fbo{nullptr};

    /// The Shader manager to use when building shaders
    Ra::Engine::Data::ShaderProgramManager* m_shaderMngr{nullptr};

    /// The color texture for output.Stored here for easy access.
    SharedTextures m_outputTexture;

    /// The custom Attrib Vertex shader function
    std::string m_customVertexAttrib{"void outputCustomAttribs(){}\n"};

    /// The custom Attrib Geometry shader function
    /// Todo : find what is the gooe profile ...
    std::string m_customGeometryAttrib{"void propagateAttributes(){}\n"};

    /// the custom FragmentColor shader function
    /// If not changed, compute the same expression (without ao) than the envlight stage
    std::string m_customFragmentColor{
        "\nvec4 computeCustomColor(Material mat, vec3 lightDir, vec3 viewDir, vec3 normal_world) { \n"
        "vec3 diffColor; \n"
        "vec3 specColor; \n"
        "getSeparateBSDFComponent( mat, getPerVertexTexCoord(), lightDir,  viewDir,\n"
        "vec3(0, 0, 1), diffColor, specColor );\n"
        "vec3 envd;\n"
        "vec3 envs;\n"
        "float r = getGGXRoughness(mat, getPerVertexTexCoord());\n"
        "int e = getEnvMapColors(r, normal_world, envd, envs);"
        "vec3 finalColor;\n"
        "if (e==1) { finalColor = diffColor*envd + specColor*envs; }\n"
        "else { finalColor = (diffColor + specColor) * max(lightDir.z, 0) \n"
        "       * lightContributionFrom(light, getWorldSpacePosition().xyz); }\n"
        "#ifdef EXPORT_VECTOR_FIELD\n"
        "out_vector_field = vec4(getWorldSpaceTangent(), 1);\n"
        "#endif\n"
        "return vec4( finalColor, 1); \n"
        "}\n"};

    /// State indicator
    mutable bool m_needConfigRebuild{true};

    /// The light manager to use
    const Ra::Engine::Scene::LightManager* m_lightmanager;

    /// The splats size
    float m_splatsSize{0.025};

    /// The Environment to used for envmap lighting
    std::shared_ptr<EnvMap> m_envmap{nullptr};

    /// The strength of the envmap
    float m_envStrength;

    /// Export a vector texture
    bool m_exportVectorField{true};
};
} // namespace RadiumNBR
