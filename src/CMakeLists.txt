add_subdirectory(libRender)

if (WITH_H3D_SUPPORT)
    add_subdirectory(libH3D)
endif (WITH_H3D_SUPPORT)

if(BUILD_PLUGIN)
    add_subdirectory(Plugin)
endif()

if(BUILD_MARA)
    set(MARA_IN_BUILD_TREE True)
    add_subdirectory(Mara EXCLUDE_FROM_ALL)
    #add_subdirectory(Mara)
endif()

if(BUILD_DEMO)
    set(VIEWER_IN_BUILD_TREE True)
    add_subdirectory(DemoApp EXCLUDE_FROM_ALL)
endif()
