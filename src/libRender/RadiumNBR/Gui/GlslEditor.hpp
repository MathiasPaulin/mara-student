#pragma once
#include <RadiumNBR/NodeBasedRendererMacro.hpp>

#include <QMainWindow>
#include <QMap>
#include <RadiumNBR/Gui/CodeEditor/CodeEditor.hpp>
#include <RadiumNBR/Gui/CodeEditor/CodeEditorHighlighter.hpp>

namespace RadiumNBR::Gui {

/**
 * Class that allow to edit glsl code.
 * @todo fix the syntax highligt configuration (for now, it the one for C++)
 * @todo add the Radium glsl interface in the keywords set for auto completion
 * @todo (not really needed) : allow to configure the style ;)
 */
class NodeBasedRenderer_LIBRARY_API GlslEditor : public QCodeEditor
{
    Q_OBJECT
  public:
    ///
    ///  @fn    Default constructor
    ///  @brief Initializes a new instance of QCodeEditor_Example.
    ///
    GlslEditor( QWidget* parent = 0 );

    ///
    ///  @fn    Destructor
    ///  @brief Frees resources used by QCodeEditor_Example.
    ///
    ~GlslEditor();

  private slots:

    ///
    ///  @fn    addMacro
    ///  @brief Adds the given macro.
    ///
    void addMacro( const QSyntaxRule& rule, QString seq, QTextBlock block );

    ///
    ///  @fn    removeMacro2
    ///  @brief Removes a macro under certain conditions.
    ///
    void removeMacro( QCodeEditorBlockData* data );

  private:
    //
    //  Class members
    //
    QMap<QTextBlock, QSyntaxRule> m_RuleMap;
    QMap<QTextBlock, QString> m_MacroMap;
};
} // namespace RadiumNBR::Gui
