#include <RadiumNBR/Passes/WireframePass.hpp>

#ifdef PASSES_LOG
#    include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log
#endif

#include <Engine/Data/Material.hpp>
#include <Engine/Data/ShaderConfigFactory.hpp>
#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/Data/Texture.hpp>
#include <Engine/Rendering/RenderObject.hpp>

#include <globjects/Framebuffer.h>

#include <Engine/Data/Mesh.hpp>
#include <Engine/Data/ViewingParameters.hpp>

namespace RadiumNBR {
using namespace gl;

static const GLenum buffers[] = {GL_COLOR_ATTACHMENT0};

WireframePass::WireframePass( const std::vector<RenderObjectPtr>* objectsToRender,
                              const Ra::Core::Utils::Index& idx ) :
    RenderPass( "Wireframe pass", idx, objectsToRender ) {}

WireframePass::~WireframePass() = default;

bool WireframePass::initializePass( size_t /* width */,
                                    size_t /* height */,
                                    Ra::Engine::Data::ShaderProgramManager* shaderMngr ) {
    m_shaderMngr = shaderMngr;
    m_fbo        = std::make_unique<globjects::Framebuffer>();

    return true;
}

void WireframePass::setInputs( const SharedTextures& depthBuffer ) {
    addImportedTextures( {"WireframePass::Depth", depthBuffer.second} );
}

void WireframePass::setOutput( const SharedTextures& colorBuffer ) {
    m_outputTexture = colorBuffer;
}

bool WireframePass::update() {
    // keep the wireframe cache coherent with the scene
    if ( m_wireframes.size() > m_objectsToRender->size() ) { m_wireframes.clear(); };
    return true;
}

template <typename IndexContainerType>
void computeIndices( Ra::Core::Geometry::LineMesh::IndexContainerType& indices,
                     IndexContainerType& other ) {

    for ( const auto& index : other )
    {
        auto s = index.size();
        for ( unsigned int i = 0; i < s; ++i )
        {
            int i1 = index[i];
            int i2 = index[( i + 1 ) % s];
            if ( i1 > i2 ) std::swap( i1, i2 );
            indices.emplace_back( i1, i2 );
        }
    }

    std::sort( indices.begin(),
               indices.end(),
               []( const Ra::Core::Geometry::LineMesh::IndexType& a,
                   const Ra::Core::Geometry::LineMesh::IndexType& b ) {
                   return a[0] < b[0] || ( a[0] == b[0] && a[1] < b[1] );
               } );
    indices.erase( std::unique( indices.begin(), indices.end() ), indices.end() );
}

// store LineMesh and Core, define the observer functor to update data one core update for wireframe
// linemesh
template <typename CoreGeometry>
class VerticesUpdater
{
  public:
    VerticesUpdater( std::shared_ptr<Ra::Engine::Data::LineMesh> disp, CoreGeometry& core ) :
        m_disp{disp}, m_core{core} {};

    void operator()() { m_disp->getCoreGeometry().setVertices( m_core.vertices() ); }
    std::shared_ptr<Ra::Engine::Data::LineMesh> m_disp;
    CoreGeometry& m_core;
};

template <typename CoreGeometry>
class IndicesUpdater
{
  public:
    IndicesUpdater( std::shared_ptr<Ra::Engine::Data::LineMesh> disp, CoreGeometry& core ) :
        m_disp{disp}, m_core{core} {};

    void operator()() {
        auto lineIndices = m_disp->getCoreGeometry().getIndicesWithLock();
        computeIndices( lineIndices, m_core.getIndices() );
        m_disp->getCoreGeometry().indicesUnlock();
    }
    std::shared_ptr<Ra::Engine::Data::LineMesh> m_disp;
    CoreGeometry& m_core;
};

// create a linemesh to draw wireframe given a core mesh
template <typename CoreGeometry>
void setupLineMesh( std::shared_ptr<Ra::Engine::Data::LineMesh>& disp, CoreGeometry& core ) {
    Ra::Core::Geometry::LineMesh lines;
    Ra::Core::Geometry::LineMesh::IndexContainerType indices;
    lines.setVertices( core.vertices() );
    computeIndices( indices, core.getIndices() );
    if ( indices.size() > 0 )
    {
        lines.setIndices( std::move( indices ) );
        disp = std::make_shared<Ra::Engine::Data::LineMesh>( std::string( "wireframe" ),
                                                             std::move( lines ) );
        disp->updateGL();
        // add observer
        auto handle = core.template getAttribHandle<typename CoreGeometry::Point>(
            Ra::Engine::Data::Mesh::getAttribName( Ra::Engine::Data::Mesh::VERTEX_POSITION ) );
        core.vertexAttribs().getAttrib( handle ).attach( VerticesUpdater( disp, core ) );
        core.attach( IndicesUpdater( disp, core ) );
    }
    else
    { disp.reset(); }
}

template <typename CoreGeometry>
void processLineMesh( const std::shared_ptr<CoreGeometry>& m,
                      std::shared_ptr<Ra::Engine::Data::LineMesh>& r ) {
    if ( m->getRenderMode() ==
         Ra::Engine::Data::AttribArrayDisplayable::MeshRenderMode::RM_TRIANGLES )
    { setupLineMesh( r, m->getCoreGeometry() ); }
}

void WireframePass::resize( size_t width, size_t height ) {

    m_width  = width;
    m_height = height;

    // Only resize the owned textures. Imported one are resized by their owner
    for ( auto& t : m_localTextures )
    {
        t.second->resize( width, height );
    }

    for ( auto& t : m_sharedTextures )
    {
        t.second->resize( width, height );
    }

    m_fbo->bind();
    m_fbo->attachTexture( GL_DEPTH_ATTACHMENT,
                          m_importedTextures["WireframePass::Depth"]->texture() );
    m_fbo->attachTexture( GL_COLOR_ATTACHMENT0, m_outputTexture.second->texture() );
#ifdef PASSES_LOG
    if ( m_fbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    { LOG( logERROR ) << "FBO Error (EmissivityPass::resize): " << m_fbo->checkStatus(); }
#endif
    // finished with fbo, undbind to bind default
    globjects::Framebuffer::unbind();
}

void WireframePass::execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const {
    m_fbo->bind();
    // only draw into 1 buffers (Color)
    glDrawBuffers( 1, buffers );
    glDepthMask( GL_FALSE );
    glColorMask( GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE );
    glEnable( GL_DEPTH_TEST );
    glDepthFunc( GL_LESS );
    glEnable( GL_BLEND );

    glBlendEquationSeparate( GL_FUNC_ADD, GL_FUNC_ADD );
    glBlendFuncSeparate( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO );
    // To prevent z-fighting, remove polygon offset
    glDisable( GL_POLYGON_OFFSET_FILL );

    for ( const auto& ro : *m_objectsToRender )
    {
        if ( ro->isVisible() )
        {
            std::shared_ptr<Ra::Engine::Data::Displayable> wro;

            auto it = m_wireframes.find( ro.get() );
            if ( it == m_wireframes.end() )
            {
                using trimesh = Ra::Engine::Data::IndexedGeometry<Ra::Core::Geometry::TriangleMesh>;
                using polymesh = Ra::Engine::Data::IndexedGeometry<Ra::Core::Geometry::PolyMesh>;
                std::shared_ptr<Ra::Engine::Data::LineMesh> disp;
                auto displayable = ro->getMesh();
                auto tm          = std::dynamic_pointer_cast<trimesh>( displayable );
                auto tp          = std::dynamic_pointer_cast<polymesh>( displayable );
                if ( tm ) { processLineMesh( tm, disp ); }
                if ( tp ) { processLineMesh( tp, disp ); }
                m_wireframes[ro.get()] = disp;
                wro                    = disp;
            }
            else
            { wro = it->second; }

            auto shader = m_shaderMngr->getShaderProgram( "WireframePass::WireframeProgram" );
            if ( wro && shader )
            {
                wro->updateGL();
                shader->bind();
                Ra::Core::Matrix4 modelMatrix = ro->getTransformAsMatrix();
                shader->setUniform( "transform.proj", viewParams.projMatrix );
                shader->setUniform( "transform.view", viewParams.viewMatrix );
                shader->setUniform( "transform.model", modelMatrix );
                shader->setUniform( "viewport", Ra::Core::Vector2{m_width, m_height} );
                wro->render( shader );
                GL_CHECK_ERROR;
            }
        }
    }
    // need to maintain global state invariant
    glEnable( GL_POLYGON_OFFSET_FILL );
    glDisable( GL_BLEND );
}

bool WireframePass::buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                                          Ra::Engine::Rendering::RenderTechnique& rt ) const {
    std::string resourcesRootDir = getResourcesDir();
    auto mat = const_cast<Ra::Engine::Rendering::RenderObject*>( ro )->getMaterial();
    // Volumes are not used in WireframePass
    if ( mat->getMaterialAspect() == Ra::Engine::Data::Material::MaterialAspect::MAT_DENSITY )
    { return false; }
    if ( auto cfg = Ra::Engine::Data::ShaderConfigurationFactory::getConfiguration(
             {"WireframePass::WireframeProgram"} ) )
    { rt.setConfiguration( *cfg, passIndex() ); }
    else
    {

        Ra::Engine::Data::ShaderConfiguration theConfig{
            {"WireframePass::WireframeProgram"},
            resourcesRootDir + "Shaders/WireframePass/Advanced/wireframe.vert.glsl",
            resourcesRootDir + "Shaders/WireframePass/Advanced/wireframe.frag.glsl"};
        theConfig.addShader( Ra::Engine::Data::ShaderType_GEOMETRY,
                             resourcesRootDir +
                                 "Shaders/WireframePass/Advanced/wireframe.geom.glsl" );
        // Add to the ShaderConfigManager
        Ra::Engine::Data::ShaderConfigurationFactory::addConfiguration( theConfig );
        // Add to the RenderTechnique
        rt.setConfiguration( theConfig, passIndex() );
    }
    rt.setParametersProvider( mat, passIndex() );
    return true;
}

} // namespace RadiumNBR
