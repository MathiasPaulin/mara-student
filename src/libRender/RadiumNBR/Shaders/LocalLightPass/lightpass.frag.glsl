/*
the functions toDiscard and getNormals are given by the implementation of the Material Interface.
The appropriate code will be added at runtime by the shader composer or the render pass initialiser.
*/
#include "DefaultLight.glsl"
#include "VertexAttribInterface.frag.glsl"

layout (location = 5) in vec3 in_viewVector;
layout (location = 6) in vec3 in_lightVector;

uniform sampler2D amb_occ_sampler;

layout (location = 0) out vec4 out_color;// Position in World Space

//------------------- main ---------------------

void main() {
    // All vectors are in world space
    // A material is always evaluated in the fragment local Frame
    // compute matrix from World to local Frame
    vec3 normalWorld     = getWorldSpaceNormal();// normalized interpolated normal
    vec3 tangentWorld    = getWorldSpaceTangent();// normalized tangent
    vec3 binormalWorld   = getWorldSpaceBiTangent();// normalized bitangent
    // discard non opaque fragment
    vec4 bc = getDiffuseColor(material, getPerVertexTexCoord());
    if (toDiscard(material, bc))
        discard;
    // Apply normal mapping
    normalWorld         = getNormal(material, getPerVertexTexCoord(),
    normalWorld, tangentWorld, binormalWorld);// normalized bump-mapped normal
    binormalWorld         = normalize(cross(normalWorld, tangentWorld));// normalized tangent
    tangentWorld         = normalize(cross(binormalWorld, normalWorld));// normalized bitangent

    mat3 world2local;
    world2local[0]  = vec3(tangentWorld.x, binormalWorld.x, normalWorld.x);
    world2local[1]  = vec3(tangentWorld.y, binormalWorld.y, normalWorld.y);
    world2local[2]  = vec3(tangentWorld.z, binormalWorld.z, normalWorld.z);
    // transform all vectors in local frame so that N = (0, 0, 1);
    vec3 lightDir = normalize(world2local * in_lightVector);// incident direction
    vec3 viewDir = normalize(world2local * in_viewVector);// outgoing direction

    vec3 bsdf    = evaluateBSDF(material, getPerVertexTexCoord(), lightDir, viewDir);

    vec3 contribution    = lightContributionFrom(light, getWorldSpacePosition().xyz);

    vec2 size = textureSize(amb_occ_sampler, 0).xy;
    vec3 ao = texture(amb_occ_sampler, gl_FragCoord.xy/size).rgb;

    out_color = vec4(bsdf * contribution  * ao, 1.0);
}
