#include <RadiumNBR/Gui/GlslEditor.hpp>

namespace RadiumNBR::Gui {

GlslEditor::GlslEditor( QWidget* parent ) : QCodeEditor( parent ) {

    // Loads the highlighting rules from a resource file
    QCodeEditorDesign design( ":/CodeEditor/EditorDefaultDesign.xml" );
    QList<QSyntaxRule> rules =
        QSyntaxRules::loadFromFile( ":/CodeEditor/HighlightRules_cpp.xml", design );

    // Creates a new code editor, sets the rules and design, adds it to the form
    setDesign( design );
    setRules( rules );
    setKeywords( {"printf", "scanf", "atoi", "mbtoa", "strlen", "memcpy", "memset"} );

    // Connects the highlighter's onMatch signal to our printMatch slot
    connect( highlighter(),
             SIGNAL( onMatch( QSyntaxRule, QString, QTextBlock ) ),
             this,
             SLOT( addMacro( QSyntaxRule, QString, QTextBlock ) ) );
    connect( highlighter(),
             SIGNAL( onRemove( QCodeEditorBlockData* ) ),
             this,
             SLOT( removeMacro( QCodeEditorBlockData* ) ) );
}

GlslEditor::~GlslEditor() {}

void GlslEditor::addMacro( const QSyntaxRule& rule, QString seq, QTextBlock block ) {
    // Add the macro, if not already existing
    if ( rule.id() == "macro" )
    {
        foreach ( const QTextBlock& b, m_RuleMap.keys() )
        {
            if ( b.userData() != NULL && block.userData() != NULL )
            {
                auto* d1 = static_cast<QCodeEditorBlockData*>( block.userData() );
                auto* d2 = static_cast<QCodeEditorBlockData*>( b.userData() );
                if ( d1->id == d2->id ) { return; }
            }
        }

        QString def = seq.split( ' ' ).at( 0 );
        m_RuleMap.insert( block, rule );
        m_MacroMap.insert( block, def );
        addKeyword( def );

        // TODO: Add highlighting rule for the new macro
    }
}

void GlslEditor::removeMacro( QCodeEditorBlockData* data ) {
    foreach ( const QTextBlock& b, m_RuleMap.keys() )
    {
        if ( b.userData() )
        {
            auto* d = static_cast<QCodeEditorBlockData*>( b.userData() );
            if ( d->id == data->id )
            {
                removeKeyword( m_MacroMap.value( b ) );
                m_RuleMap.remove( b );
                m_MacroMap.remove( b );
            }
        }
    }
}
} // namespace RadiumNBR::Gui
