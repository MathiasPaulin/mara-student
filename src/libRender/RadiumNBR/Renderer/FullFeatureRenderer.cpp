#include <RadiumNBR/Renderer/FullFeatureRenderer.hpp>

#include <Core/Containers/MakeShared.hpp>
#include <Core/Resources/Resources.hpp>

#ifdef PASSES_LOG
#    include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log
#endif

#include <Engine/Data/Material.hpp>
#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/Rendering/RenderObject.hpp>
#include <Engine/Scene/DefaultCameraManager.hpp>
#include <Engine/Scene/DefaultLightManager.hpp>

#include <RadiumNBR/Passes/AccessibilityBufferPass.hpp>
#include <RadiumNBR/Passes/ClearPass.hpp>
#include <RadiumNBR/Passes/EmissivityPass.hpp>
#include <RadiumNBR/Passes/EnvLightPass.hpp>
#include <RadiumNBR/Passes/GeomPrepass.hpp>
#include <RadiumNBR/Passes/LocalLightPass.hpp>
#include <RadiumNBR/Passes/TransparencyPass.hpp>
#include <RadiumNBR/Passes/VolumePass.hpp>
#include <RadiumNBR/Passes/WireframePass.hpp>

#include <globjects/Framebuffer.h>

using namespace Ra::Engine;
using namespace Ra::Engine::Scene;
using namespace Ra::Engine::Data;
using namespace Ra::Engine::Rendering;

namespace RadiumNBR {
using namespace gl;

static int FullFeaturedRendererMagic = 0x0F0F0F0F;

static const GLenum buffers[] = {GL_COLOR_ATTACHMENT0,
                                 GL_COLOR_ATTACHMENT1,
                                 GL_COLOR_ATTACHMENT2,
                                 GL_COLOR_ATTACHMENT3,
                                 GL_COLOR_ATTACHMENT4,
                                 GL_COLOR_ATTACHMENT5,
                                 GL_COLOR_ATTACHMENT6,
                                 GL_COLOR_ATTACHMENT7};

FullFeatureRenderer::FullFeatureRenderer() : NodeBasedRenderer() {}

FullFeatureRenderer::~FullFeatureRenderer() = default;

void FullFeatureRenderer::initializeInternal() {
    NodeBasedRenderer::initializeInternal();
    initPasses();
    for ( const auto& t : sharedTextures() )
    {
        m_secondaryTextures.insert( {t.first, t.second.get()} );
    }
}

void FullFeatureRenderer::initPasses() {
    auto resourcesCheck = Ra::Core::Resources::getResourcesPath(
        reinterpret_cast<void*>( &FullFeaturedRendererMagic ), {"Resources/RadiumNBR"} );
    if ( !resourcesCheck )
    {
        LOG( Ra::Core::Utils::logERROR ) << "Unable to find resources for NodeBasedRenderer!";
        return;
    }
    auto resourcesPath{*resourcesCheck};
    auto depthTexture = sharedTextures().find( "Depth (RadiumNBR)" );
    auto colorTexture = sharedTextures().find( "Linear RGB (RadiumNBR)" );

    /***
     * 0 - clear the final picture
     *  Set the background color either to the global background color or to the env-map
     */
    {
        m_clearPass =
            std::make_shared<ClearPass>( nullptr, FullFeaturedRendererPasses::CLEAR_PASS );
        m_clearPass->setOutput( *colorTexture );
        m_clearPass->setBackground( getBackgroundColor() );
        m_clearPass->initializePass( m_width, m_height, m_shaderProgramManager );
    }

    /***
     * 1 Extract basic geometric information : position, normal and depth
     *  Only draw ont AOV Position and Normal
     */
    {
        // Zprepass takes all objects but transparent objects are expected to render only their
        // opaque fragments.
        m_zPrePass =
            std::make_shared<GeomPrePass>( allRenderObjects(), FullFeaturedRendererPasses::Z_PASS );
        // Add the shared depth texture
        m_zPrePass->setOutput( *depthTexture );
        // configure acces to shader files
        m_zPrePass->setResourcesDir( resourcesPath );
        m_zPrePass->initializePass( m_width, m_height, m_shaderProgramManager );
        for ( auto&& t : m_zPrePass->getOutputImages() )
        {
            sharedTextures().insert( t );
        }
    }

    /***
     * Compute accessibiity buffer (ambient occlusion) from output of pass 1
     *  Screen space ambiant occlusion
     */
    {
        auto sphereSampler =
            std::make_unique<SphereSampler>( m_aoSamplingMethod, m_aoSamplingPoints );
        m_aoPass = std::make_shared<AccessibilityBufferPass>(
            allRenderObjects(), FullFeaturedRendererPasses::ACCESSIBILITY_PASS );
        m_aoPass->setResourcesDir( resourcesPath );
        m_aoPass->setSampler( std::move( sphereSampler ) );
        // Connect to the preceeding pass
        auto posTexture = sharedTextures().find( "GeomPrePass::PosInWorld" );
        auto nrmTexture = sharedTextures().find( "GeomPrePass::NormalInWorld" );
        m_aoPass->setInputs( *posTexture, *nrmTexture );
        m_aoPass->initializePass( m_width, m_height, m_shaderProgramManager );
        for ( auto&& t : m_aoPass->getOutputImages() )
        {
            sharedTextures().insert( t );
        }
    }
    auto ambientOcclusionTexture = sharedTextures().find( "SSDO::AOBuffer" );
    /***
     * 3 - Render emissivity
     *  Render Ambiant or emissivity color.
     *      Ambiant is 0.01 the base color. TODO -> set 0.01 a parameter of the pass
     *      Do not blend with output, replace the background color by the emissivity/ambiant color
     */
    {
        m_emissivityPass = std::make_shared<EmissivityPass>(
            allRenderObjects(), FullFeaturedRendererPasses::EMISSIVITY_PASS );
        m_emissivityPass->setResourcesDir( resourcesPath );
        m_emissivityPass->setInputs( *depthTexture, *ambientOcclusionTexture );
        m_emissivityPass->setOutput( *colorTexture );
        m_emissivityPass->setBackground( getBackgroundColor() );
        m_emissivityPass->initializePass( m_width, m_height, m_shaderProgramManager );
    }
    /***
     * 4 - Render envlighting
     *      Only render scene if an envmap is set
     *      Blend with the previous color buffer
     */
    {
        m_envlightPass = std::make_shared<EnvLightPass>(
            allRenderObjects(), FullFeaturedRendererPasses::ENVMAP_LIGHTING_OPAQUE_PASS );
        m_envlightPass->setResourcesDir( resourcesPath );
        m_envlightPass->setInputs( *depthTexture, *ambientOcclusionTexture );
        m_envlightPass->setOutput( *colorTexture );
        m_envlightPass->initializePass( m_width, m_height, m_shaderProgramManager );
    }

    /***
     * 5 - Compute lighting - Opaque objects
     */
    {
        m_locallightPass = std::make_shared<LocalLightPass>(
            allRenderObjects(), FullFeaturedRendererPasses::LIGHTING_OPAQUE_PASS );
        m_locallightPass->setResourcesDir( resourcesPath );
        m_locallightPass->setInputs( *depthTexture, *ambientOcclusionTexture );
        m_locallightPass->setOutput( *colorTexture );
        m_locallightPass->setLightManager( m_lightmanagers[0] );
        m_locallightPass->initializePass( m_width, m_height, m_shaderProgramManager );
    }
    /***
     * 6 - Compute lighting - Transparent objects
     */
    {
        m_transparencyPass = std::make_shared<TransparencyPass>(
            &m_transparentRenderObjects, FullFeaturedRendererPasses::LIGHTING_TRANSPARENT_PASS );
        m_transparencyPass->setResourcesDir( resourcesPath );
        m_transparencyPass->setInputs( *depthTexture, *ambientOcclusionTexture );
        m_transparencyPass->setOutput( *colorTexture );
        m_transparencyPass->setLightManager( m_lightmanagers[0] );
        m_transparencyPass->initializePass( m_width, m_height, m_shaderProgramManager );
    }

    /***
     * 7 - Compute lighting for volume objects
     */
    {
        m_volumelightPass = std::make_shared<VolumeLightingPass>(
            &m_volumetricRenderObjects, FullFeaturedRendererPasses::LIGHTING_VOLUME_PASS );
        m_volumelightPass->setResourcesDir( resourcesPath );
        m_volumelightPass->setInputs( *depthTexture, *colorTexture );
        m_volumelightPass->setOutput( *colorTexture );
        m_volumelightPass->setLightManager( m_lightmanagers[0] );
        m_volumelightPass->initializePass( m_width, m_height, m_shaderProgramManager );
    }

    /***
     * Option - Render in wireframe
     *  Render all objects as wireframe
     *      Render in white over an existing depth buffer
     */
    {
        m_wireframePass = std::make_shared<WireframePass>(
            allRenderObjects(), FullFeaturedRendererPasses::WIREFRAME_PASS );
        m_wireframePass->setResourcesDir( resourcesPath );
        m_wireframePass->setInputs( *depthTexture );
        m_wireframePass->setOutput( *colorTexture );
        m_wireframePass->initializePass( m_width, m_height, m_shaderProgramManager );
    }

    // Build the sequence of active passes
    addPass( m_clearPass, m_clearPass->index() );
    m_clearPass->activate();
    addPass( m_zPrePass, m_zPrePass->index() );
    m_zPrePass->activate();
    addPass( m_aoPass, m_aoPass->index() );
    m_aoPass->activate();

    addPass( m_emissivityPass, m_emissivityPass->index() );
    m_emissivityPass->activate();

    addPass( m_envlightPass, m_envlightPass->index() );
    m_envlightPass->activate();

    addPass( m_locallightPass, m_locallightPass->index(), true );
    m_locallightPass->activate();
    addPass( m_transparencyPass, m_transparencyPass->index() );
    m_transparencyPass->activate();
    addPass( m_volumelightPass, m_volumelightPass->index() );
    m_volumelightPass->activate();

    addPass( m_wireframePass, m_wireframePass->index() );

    // Add intermediate textures to the texture explorer
    for ( const auto& p : renderPasses() )
    {
        for ( auto&& t : p.second->getOutputImages() )
        {
            sharedTextures().insert( t );
        }
    }
}

// Todo : verify if the ro partition is the good one and that the passes use the right part.
void FullFeatureRenderer::updateStepInternal( const ViewingParameters& renderData ) {
    // Split objects into opaque and transparent
    m_transparentRenderObjects.clear();
    m_volumetricRenderObjects.clear();

    auto objectsToRender = allRenderObjects();
    for ( auto it = objectsToRender->begin(); it != objectsToRender->end(); )
    {
        if ( ( *it )->isTransparent() )
        {
            m_transparentRenderObjects.push_back( *it );
            ++it;
        }
        else
        {
            auto material = ( *it )->getMaterial();
            if ( material &&
                 material->getMaterialAspect() == Material::MaterialAspect::MAT_DENSITY )
            {
                m_volumetricRenderObjects.push_back( *it );
                it = objectsToRender->erase( it );
            }
            else
            { ++it; }
        }
    }
    m_clearPass->setBackground( getBackgroundColor() );
    if ( m_hasEnvMap )
    {
        // for ( auto &lm : m_lightmanagers ) { lm->removeSystemLights(); }
    }
    NodeBasedRenderer::updateStepInternal( renderData );
}

void FullFeatureRenderer::setEnvMap( const std::string& files ) {
    if ( files.empty() )
    {
        m_clearPass->setEnvMap( nullptr );
        m_envlightPass->setEnvMap( nullptr );
        m_envlightPass->deactivate();
        m_volumelightPass->setEnvMap( nullptr );
        m_hasEnvMap = false;
    }
    else
    {
        // Todo, add type VERTICALCROSS, HORIZONTALCROSS and differentiate between all types
        //  using file extension and file size
        auto t = ( files.find( ';' ) != files.npos ) ? EnvMap::EnvMapType::ENVMAP_CUBE
                                                     : EnvMap::EnvMapType::ENVMAP_PFM;
        if ( t == EnvMap::EnvMapType::ENVMAP_PFM )
        {
            auto ext = files.substr( files.size() - 3 );
            if ( ext != "pfm" ) { t = EnvMap::EnvMapType::ENVMAP_LATLON; }
        }
        // for now, only skyboxes are managed
        auto e = std::make_shared<EnvMap>( files, t, true );
        m_clearPass->setEnvMap( e );
        m_envlightPass->setEnvMap( e );
        // activate the envmap pass only if wireframe is inactive ?
        if ( !m_wireframePass->isActive() ) { m_envlightPass->activate(); }
        m_volumelightPass->setEnvMap( e );
        m_hasEnvMap = true;
    }
}

void FullFeatureRenderer::showEnvMap( bool state ) {
    m_clearPass->showEnvMap( state );
}

void FullFeatureRenderer::setEnvStrength( int s ) {
    m_envlightPass->setEnvStrength( s );
}

Scalar FullFeatureRenderer::getAoRadius() const {
    if ( m_aoPass ) { return m_aoPass->aoRadius(); }
    return 5.0;
}
void FullFeatureRenderer::setAoRadius( Scalar r ) {
    if ( m_aoPass ) { m_aoPass->setAoRadius( r ); }
}

int FullFeatureRenderer::getAoSamplingDensity() const {
    return m_aoSamplingPoints;
}
void FullFeatureRenderer::setAoSamplingDensity( int d ) {
    m_aoSamplingPoints = d;
    auto sphereSampler = std::make_unique<SphereSampler>( m_aoSamplingMethod, m_aoSamplingPoints );
    if ( m_aoPass ) { m_aoPass->setSampler( std::move( sphereSampler ) ); }
}

void FullFeatureRenderer::wireframeMode( bool status ) {
    enableWireframe( status );
    if ( m_wireframe )
    {
        m_wireframePass->activate();

        m_aoPass->deactivate();
        m_emissivityPass->deactivate();
        m_envlightPass->deactivate();
        m_locallightPass->deactivate();
        m_transparencyPass->deactivate();
        m_volumelightPass->deactivate();
    }
    else
    {
        m_wireframePass->deactivate();

        m_aoPass->activate();
        m_emissivityPass->activate();
        m_envlightPass->activate();
        m_locallightPass->activate();
        m_transparencyPass->activate();
        m_volumelightPass->activate();
    }
}
} // namespace RadiumNBR
