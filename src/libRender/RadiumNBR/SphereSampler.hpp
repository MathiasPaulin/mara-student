#pragma once
#include <RadiumNBR/NodeBasedRendererMacro.hpp>

#include <Core/Containers/VectorArray.hpp>
#include <Core/Types.hpp>

#include <Engine/Data/Texture.hpp>

#include <random>

namespace RadiumNBR {

Scalar constexpr sqrtNewtonRaphsonhelper(Scalar x, Scalar curr, Scalar prev)
{
    return curr == prev
    ? curr
    : sqrtNewtonRaphsonhelper(x, 0.5_ra * (curr + x / curr), curr);
}
Scalar constexpr ct_sqrt(Scalar x){
    return sqrtNewtonRaphsonhelper( x, x, 0_ra );
}

/// Implements the 2D fibonacci sequence
/// (i, N) => [i / phi, i / N]
/// where phi = (1 + sqrt(5)) / 2
class NodeBasedRenderer_LIBRARY_API FibonacciSequence
{

    static constexpr Scalar phi = ( 1_ra + ct_sqrt( 5_ra ) ) / 2_ra;
    int n;

  public:
    explicit FibonacciSequence( int number ) : n{std::max( 5, number )} {};
    // copyable
    FibonacciSequence( const FibonacciSequence& ) = default;
    FibonacciSequence& operator=( const FibonacciSequence& ) = default;
    // movable
    FibonacciSequence( FibonacciSequence&& ) = default;
    FibonacciSequence& operator=( FibonacciSequence&& ) = default;
    virtual ~FibonacciSequence()                        = default;

    int range() { return n; }
    Scalar operator()( unsigned int i ) { return Scalar( i ) / phi; }
};

/// 1D Van der Corput sequence
/// only implemented for 32bits floats (converted out to Scalar)
struct NodeBasedRenderer_LIBRARY_API VanDerCorputSequence {
    Scalar operator()( unsigned int bits ) {
        bits = ( bits << 16u ) | ( bits >> 16u );
        bits = ( ( bits & 0x55555555u ) << 1u ) | ( ( bits & 0xAAAAAAAAu ) >> 1u );
        bits = ( ( bits & 0x33333333u ) << 2u ) | ( ( bits & 0xCCCCCCCCu ) >> 2u );
        bits = ( ( bits & 0x0F0F0F0Fu ) << 4u ) | ( ( bits & 0xF0F0F0F0u ) >> 4u );
        bits = ( ( bits & 0x00FF00FFu ) << 8u ) | ( ( bits & 0xFF00FF00u ) >> 8u );
        return Scalar( float( bits ) * 2.3283064365386963e-10 ); // / 0x100000000
    }
};

/// Implements the 2D fibonacci Point set
/// points follow the FibonacciSequence
class NodeBasedRenderer_LIBRARY_API FibonacciPointSet
{
    FibonacciSequence seq;

  public:
    explicit FibonacciPointSet( int n ) : seq( n ){};
    // copyable
    FibonacciPointSet( const FibonacciPointSet& ) = default;
    FibonacciPointSet& operator=( const FibonacciPointSet& ) = default;
    // movable
    FibonacciPointSet( FibonacciPointSet&& ) = default;
    FibonacciPointSet& operator=( FibonacciPointSet&& ) = default;
    virtual ~FibonacciPointSet()                        = default;

    int range() { return seq.range(); }
    Ra::Core::Vector2 operator()( unsigned int i ) {
        return {seq( i ), Scalar( i ) / Scalar( range() )};
    }
};

/// 2D Hammersley point set
class NodeBasedRenderer_LIBRARY_API HammersleyPointSet
{
    VanDerCorputSequence seq;
    int n;

  public:
    explicit HammersleyPointSet( int number ) : n( number ){};
    // copyable
    HammersleyPointSet( const HammersleyPointSet& ) = default;
    HammersleyPointSet& operator=( const HammersleyPointSet& ) = default;
    // movable
    HammersleyPointSet( HammersleyPointSet&& ) = default;
    HammersleyPointSet& operator=( HammersleyPointSet&& ) = default;
    virtual ~HammersleyPointSet()                         = default;

    int range() { return n; }
    Ra::Core::Vector2 operator()( unsigned int i ) {
        return {Scalar( i ) / Scalar( range() ), seq( i )};
    }
};

/// 2D Random point set
class NodeBasedRenderer_LIBRARY_API MersenneTwisterPointSet
{
    std::mt19937 gen;
    std::uniform_real_distribution<> seq;
    int n;

  public:
    explicit MersenneTwisterPointSet( int number ) : gen( 0 ), seq( 0., 1. ), n( number ){};
    // copyable
    MersenneTwisterPointSet( const MersenneTwisterPointSet& ) = default;
    MersenneTwisterPointSet& operator=( const MersenneTwisterPointSet& ) = default;
    // movable
    MersenneTwisterPointSet( MersenneTwisterPointSet&& ) = default;
    MersenneTwisterPointSet& operator=( MersenneTwisterPointSet&& ) = default;
    virtual ~MersenneTwisterPointSet()                              = default;

    int range() { return n; }
    Ra::Core::Vector2 operator()( unsigned int i ) { return {seq( gen ), seq( gen )}; }
};

// https://observablehq.com/@mbostock/spherical-fibonacci-lattice
// http://holger.dammertz.org/stuff/notes_HammersleyOnHemisphere.html
/// Map a [0, 1)^2 point set on the unit sphere
template <class PointSet>
class NodeBasedRenderer_LIBRARY_API SphericalPointSet
{
    PointSet p;
    Ra::Core::Vector3 projectOnSphere( const Ra::Core::Vector2&& pt ) {
        Scalar theta = std::acos( 2 * pt[1] - 1 ); // 0 <= tetha <= pi
        Scalar phi   = 2_ra * Scalar( M_PI ) * pt[0];
        return {std::sin( theta ) * std::cos( phi ),
                std::sin( theta ) * std::sin( phi ),
                std::cos( theta )};
    }

  public:
    explicit SphericalPointSet( int n ) : p( n ) {}
    // copyable
    SphericalPointSet( const SphericalPointSet& ) = default;
    SphericalPointSet& operator=( const SphericalPointSet& ) = default;
    // movable
    SphericalPointSet( SphericalPointSet&& ) = default;
    SphericalPointSet& operator=( SphericalPointSet&& ) = default;
    virtual ~SphericalPointSet()                        = default;

    int range() { return p.range(); }
    Ra::Core::Vector3 operator()( unsigned int i ) { return projectOnSphere( p( i ) ); }
};

/// Sample the volume of a sphere as a random displacement of its sampled surface
class NodeBasedRenderer_LIBRARY_API SphereSampler
{
  public:
    /// Available samplers
    enum class SamplingMethod {
        FIBONACCI,  ///<- Sample the sphere with a fibonacci sequence
        HAMMERSLEY, ///<- Sample the sphere using Hammersley Point Set
        RANDOM,     ///<-- Sample the sphere using uniform random sequence
        GEODESIC,   ///<- Sample the sphere by subdivision of an icosahedron
    };
    /// Generate a sequence of point distributed on the sphere
    /// @param method the sampling method to use (@see SamplingMethod)
    /// @param level the number of point to generate
    explicit SphereSampler( SamplingMethod method, int level = 0 );
    SphereSampler( const SphereSampler& ) = delete;
    SphereSampler& operator=( const SphereSampler& ) = delete;
    SphereSampler( SphereSampler&& )                 = delete;
    SphereSampler& operator=( SphereSampler&& ) = delete;
    /// destructor
    ~SphereSampler() = default;

    /// Get the sampling scheme as a Radium texture
    Ra::Engine::Data::Texture* asTexture();

    /// Return the number of samples generated
    [[nodiscard]] int nbSamples() const { return m_nbPoints; }

  private:
    /// Texture generated on demand to be used with an OpenGL shader
    std::unique_ptr<Ra::Engine::Data::Texture> m_texture{nullptr};
    /// Final number of points
    int m_nbPoints;
    /// The points on the sphere
    Ra::Core::VectorArray<Ra::Core::Vector4> m_points;
};
} // namespace RadiumNBR
