#pragma once

#include <Core/RaCore.hpp>
/// Defines the correct macro to export dll symbols.
#if defined NodeRendererPlugin_EXPORTS
#    define NodeRenderer_PLUGIN_API DLL_EXPORT
#else
#    define NodeRendererPlugin_PLUGIN_API DLL_IMPORT
#endif
