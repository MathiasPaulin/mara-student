#include <iostream>

#include <Gui/ControlDialogWindow.hpp>
#include <QFileInfo>
#include <QVBoxLayout>
namespace Mara {
ControlDialogWindow::ControlDialogWindow( QWidget* parent ) : QDialog( parent ) {

    m_tabWidget                 = new QTabWidget;
    m_tabInfo                   = new InformationTab();
    m_tabIndices["Information"] = m_tabWidget->addTab( m_tabInfo, tr( "Information" ) );
    m_buttonBox                 = new QDialogButtonBox( QDialogButtonBox::Ok );

    connect( m_buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept );
    auto mainLayout = new QVBoxLayout;
    mainLayout->addWidget( m_tabWidget );
    mainLayout->addWidget( m_buttonBox );
    setLayout( mainLayout );

    setWindowTitle( tr( "Application Control" ) );
}

void ControlDialogWindow::setFileInfo( const std::string& fileName,
                                       const std::tuple<size_t, size_t, size_t>& sceneStats ) {
    m_tabInfo->setFileInfo( fileName.c_str(), sceneStats );
}

void ControlDialogWindow::addTab( QWidget* tab, const std::string& tabname ) {
    m_tabIndices[tabname] = m_tabWidget->addTab( tab, tabname.c_str() );
}

void ControlDialogWindow::showTab( const std::string& tabname ) {
    //    m_tabWidget->setTabEnabled(m_tabIndices[tabname], true);
    m_tabWidget->setCurrentIndex( m_tabIndices[tabname] );
    show();
}

InformationTab::InformationTab( QWidget* parent ) : QWidget( parent ) {
    // Layout of the tab
    auto fileLayout          = new QHBoxLayout();
    auto pathLayout          = new QHBoxLayout();
    auto sizeLayout          = new QHBoxLayout();
    auto objectCountLayout   = new QHBoxLayout();
    auto polyCountLayout     = new QHBoxLayout();
    auto verticesCountLayout = new QHBoxLayout();

    auto mainLayout = new QVBoxLayout;
    mainLayout->addLayout( fileLayout );
    mainLayout->addLayout( pathLayout );
    mainLayout->addLayout( sizeLayout );
    mainLayout->addLayout( objectCountLayout );
    mainLayout->addLayout( polyCountLayout );
    mainLayout->addLayout( verticesCountLayout );
    mainLayout->addStretch();
    setLayout( mainLayout );

    // Filling the tab
    auto fileNameLabel = new QLabel( tr( "File Name:" ) );
    m_fileNameDisplay  = new QLabel( "none" );
    // m_fileNameDisplay->setFrameStyle( QFrame::StyledPanel | QFrame::Sunken );
    fileLayout->addWidget( fileNameLabel );
    fileLayout->addWidget( m_fileNameDisplay );
    fileLayout->addStretch();

    auto pathLabel    = new QLabel( tr( "Path:" ) );
    m_pathNameDisplay = new QLabel( "none" );
    // m_pathNameDisplay->setFrameStyle( QFrame::Sunken);
    pathLayout->addWidget( pathLabel );
    pathLayout->addWidget( m_pathNameDisplay );
    pathLayout->addStretch();

    auto sizeLabel     = new QLabel( tr( "Size:" ) );
    m_sizeValueDisplay = new QLabel( tr( "%1 K" ).arg( 0 ) );
    // m_sizeValueDisplay->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    sizeLayout->addWidget( sizeLabel );
    sizeLayout->addWidget( m_sizeValueDisplay );
    sizeLayout->addStretch();

    auto objCountLabel = new QLabel( tr( "Object count:" ) );
    m_objCount         = new QLabel( tr( "%1" ).arg( 0 ) );
    objectCountLayout->addWidget( objCountLabel );
    objectCountLayout->addWidget( m_objCount );
    objectCountLayout->addStretch();

    auto polyCountLabel = new QLabel( tr( "Polygon count:" ) );
    m_polyCount         = new QLabel( tr( "%1" ).arg( 0 ) );
    polyCountLayout->addWidget( polyCountLabel );
    polyCountLayout->addWidget( m_polyCount );
    polyCountLayout->addStretch();

    auto vertCountLabel = new QLabel( tr( "Vertex count:" ) );
    m_vertCount         = new QLabel( tr( "%1" ).arg( 0 ) );
    verticesCountLayout->addWidget( vertCountLabel );
    verticesCountLayout->addWidget( m_vertCount );
    verticesCountLayout->addStretch();
}

void InformationTab::setFileInfo( const QString& fileName,
                                  const std::tuple<size_t, size_t, size_t>& sceneStats ) {
    QFileInfo fileInfo( fileName );
    m_fileNameDisplay->setText( fileInfo.fileName() );
    m_pathNameDisplay->setText( fileInfo.absolutePath() );
    qlonglong size = fileInfo.size() / 1024;
    m_sizeValueDisplay->setText( tr( "%1 K" ).arg( size ) );

    m_objCount->setText( tr( "%1" ).arg( std::get<0>( sceneStats ) ) );
    m_polyCount->setText( tr( "%1" ).arg( std::get<1>( sceneStats ) ) );
    m_vertCount->setText( tr( "%1" ).arg( std::get<2>( sceneStats ) ) );
}
} // namespace Mara
