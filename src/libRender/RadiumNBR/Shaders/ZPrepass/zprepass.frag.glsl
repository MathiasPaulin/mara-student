/*
the functions toDiscard and getNormals are given by the implementation of the Material Interface.
The appropriate code will be added at runtime by the shader composer or the render pass initialiser.
*/

#include "VertexAttribInterface.frag.glsl"

layout (location = 0) out vec4 out_worldpos;
layout (location = 1) out vec4 out_normal;

void main() {
    vec3 worldNormal = getNormal(material, getPerVertexTexCoord(), getWorldSpaceNormal(), getWorldSpaceTangent(), getWorldSpaceBiTangent());
    // discard non opaque fragment
    vec4 bc = getBaseColor(material, getPerVertexTexCoord());
    if (toDiscard(material, bc)) {
        discard;
    }

    out_worldpos    = getWorldSpacePosition();
    out_normal        = vec4(worldNormal * 0.5 + 0.5, 1.0);
}
