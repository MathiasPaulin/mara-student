/*
the functions toDiscard and getNormals are given by the implementation of the Material Interface.
The appropriate code will be added at runtime by the shader composer or the render pass initialiser.
*/


// Include the VertexAttribInterface so that all materials might be OK.
#include "VertexAttribInterface.frag.glsl"
const float OneOverPi = 0.3183098862;

layout (location = 5) in vec3 in_viewVector;

uniform sampler2D amb_occ_sampler;

uniform samplerCube envTexture;
uniform int numLod;

uniform int hasShEnvironment;
uniform mat4 redShCoeffs;
uniform mat4 greenShCoeffs;
uniform mat4 blueShCoeffs;


layout (location = 0) out vec4 out_color;// Position in World Space

// For debug, to be removed once finalized.
uniform float envStrength;


//------------------- main ---------------------
void main() {
    vec4 normalWorld    = vec4(getNormal(material, getPerVertexTexCoord(),
    getWorldSpaceNormal(), getWorldSpaceTangent(), getWorldSpaceBiTangent()), 1);
    // discard non opaque fragment
    //vec4 bc = getDiffuseColor(material, getPerVertexTexCoord());
    vec4 bc = getBaseColor(material, getPerVertexTexCoord());
    if (toDiscard(material, bc)) {
        discard;
    }

    vec3 diffuse;
    vec3 specular;
    vec3 view = normalize(in_viewVector);
    vec3 rfl = reflect(-view, normalWorld.xyz);

    if (getSeparateBSDFComponent(material, getPerVertexTexCoord(), rfl, view, normalWorld.xyz, diffuse, specular) == 1) {
        vec2 size = textureSize(amb_occ_sampler, 0).xy;
        vec3 ao = texture(amb_occ_sampler, gl_FragCoord.xy/size).rgb;
        bc.rgb = diffuse;
        bc.r *= dot(normalWorld, redShCoeffs * normalWorld);
        bc.g *= dot(normalWorld, greenShCoeffs * normalWorld);
        bc.b *= dot(normalWorld, blueShCoeffs * normalWorld);
        // Specular envmap
        float cosTi = clamp(dot(rfl, normalWorld.xyz), 0.001, 1.);
        vec3 spec = clamp(specular * cosTi * OneOverPi * 0.5, 0.001, 1.);
        float r = getGGXRoughness(material, getPerVertexTexCoord()) * numLod;
        bc.rgb += textureLod(envTexture, rfl, r).rgb  * spec;
        bc.rgb *= ao * envStrength;
    } else {
        bc.rgb = vec3(0);
    }

    out_color = vec4(bc.rgb, 1);
}
